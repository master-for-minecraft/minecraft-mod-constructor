package com.gdcompany.minemodconstructor.network.interceptors;

import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Response;

public class ReceivedCookiesInterceptor implements Interceptor {

	@NotNull
	@Override
	public Response intercept(Chain chain) throws IOException {
		Response originalResponse = chain.proceed(chain.request());

		if (!originalResponse.headers("Set-Cookie").isEmpty()) {
			HashSet<String> cookies = new HashSet<>(originalResponse.headers("Set-Cookie"));
			for (String cookie : cookies) {
				String[] keyValuePairs = TextUtils.split(cookie, ";");
				for (String keyValuePair : keyValuePairs) {
					String[] keyValue = TextUtils.split(keyValuePair, "=");
					if (keyValue.length == 2) {
						String keyCookie = (String) Array.get(keyValue, 0);
						String valueCookie = (String) Array.get(keyValue, 1);
//						if (AccountInstance.SESSION_ID.getKey().equals(keyCookie)) {
//							PowerPreference.getDefaultFile().setString(AccountInstance.SESSION_ID.getKey(), valueCookie);
//							Log.d("SAVE HEADER", "SESION ID: " + valueCookie);
//						}
//						if (AccountInstance.TOKEN.getKey().equals(keyCookie)) {
//							PowerPreference.getDefaultFile().setString(AccountInstance.TOKEN.getKey(), valueCookie);
//							Log.d("SAVE HEADER ", "TOKEN ID:" + valueCookie);
//						}
					}
				}
			}
		}

		return originalResponse;
	}
}
