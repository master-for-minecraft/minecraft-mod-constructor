package com.gdcompany.minemodconstructor.network;
import com.gdcompany.minemodconstructor.model.ErrorCode;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/*import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BasicNetworkCallback<T> implements Callback<T> {

    private final INetworkResult<T> mCallback;

    public BasicNetworkCallback(INetworkResult<T> callback) {
        this.mCallback = callback;
    }

    @Override
    public void onResponse(@NotNull Call<T> call, @NotNull Response<T> response) {
        int responseCode = response.code();
        BasicNetworkResponse<T> basicNetworkResponse = new BasicNetworkResponse<>(
                response.body(),
                responseCode,
                response.headers(),
                response.message(),
                response.raw().request()
        );
        boolean success = responseCode == 200 || responseCode == 201;

        String messageByCode = ErrorCode.getMessageByCode(responseCode);
        Throwable throwable;
        if (success) {
            mCallback.onSucccess(basicNetworkResponse);
        } else {
            if (messageByCode.equals(ErrorCode.UNKNOWN_ERROR.getMessage())) {
                if (response.errorBody() != null) {
                    try {
                        String responseBody = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(responseBody);
                        throwable = new Throwable(jsonObject.getJSONObject("responseError").get("errShow").toString());
                    } catch (IOException | JSONException e) {
                        throwable = new Throwable("Неизвестная ошибка");
                    }
                } else {
                    throwable = new Throwable(response.message());
                }
            } else {
                throwable = new Throwable(messageByCode);
            }
            if (responseCode == ErrorCode.UNAUTHORIZED.getCode()) {
            }
            onFailure(call, throwable);
            return;
        }

        mCallback.onFinish(true, messageByCode);
    }

    @Override
    public void onFailure(@NotNull Call<T> call, @NotNull Throwable t) {
        mCallback.onError();
        if (t.getMessage().startsWith("Unable to resolve host")) {
            mCallback.onFinish(false, "Нет соединения с Интернетом");
        } else if (t.getMessage().startsWith("timeout")) {
            mCallback.onFinish(false, "Превышено время ожидания от сервера");
        } else {
            mCallback.onFinish(false, t.getMessage());
        }
    }
}*/
