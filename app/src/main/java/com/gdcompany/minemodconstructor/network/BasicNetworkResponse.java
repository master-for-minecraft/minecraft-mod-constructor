package com.gdcompany.minemodconstructor.network;

import okhttp3.Headers;
import okhttp3.Request;

public class BasicNetworkResponse<T> {

	private final T responseBody;
	private final int code;
	private final Headers headers;
	private final String message;
	private final Request request;

	BasicNetworkResponse(T responseBody, int code, Headers headers, String message, Request request) {
		this.responseBody = responseBody;
		this.code = code;
		this.headers = headers;
		this.message = message;
		this.request = request;
	}

	public T getResponseBody() {
		return responseBody;
	}

}
