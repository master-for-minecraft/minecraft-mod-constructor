package com.gdcompany.minemodconstructor.network.interceptors;

import com.gdcompany.minemodconstructor.BuildConfig;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AddCookiesInterceptor implements Interceptor {

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
//        ArrayDeque<String> cookieArray = new ArrayDeque<>(2);
//
//        String sessionIdKey = AccountInstance.SESSION_ID.getKey();
//        String tokenKey = AccountInstance.TOKEN.getKey();
//
//        String sessionId = PowerPreference.getDefaultFile().getString(sessionIdKey);
//        String token = PowerPreference.getDefaultFile().getString(tokenKey);
//
//        if (sessionId != null && !sessionId.isEmpty()) {
//            cookieArray.add(sessionIdKey + "=" + sessionId);
//        }
//
//        if (token != null && !token.isEmpty()) {
//            cookieArray.add(tokenKey + "=" + token);
//        }
//
//        cookieArray.add("cabinetAbonentPlusRememberMe=true");
//
//        final String unifiedAccountInfoId = AccountInstance.getUnifiedAccountInfoId();
//        if (unifiedAccountInfoId != null && !unifiedAccountInfoId.isEmpty()) {
//            cookieArray.add("selectedUnifiedAccountId=" + unifiedAccountInfoId);
//        }
//
//        String cookieString = TextUtils.join("; ", cookieArray);
//        builder.addHeader("Cookie", cookieString);
//        Logger.v("OkHttp", "Adding Header: " + cookieString);
        return chain.proceed(builder.build());
    }
}
