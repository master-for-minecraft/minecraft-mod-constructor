package com.gdcompany.minemodconstructor;
import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;

import com.gdcompany.minemodconstructor.instance.MobileAdsInstance;
import com.gdcompany.minemodconstructor.utils.AppOpenManager;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.preference.PowerPreference;

import java.util.List;

public class MainApplication extends Application {

	private static MainApplication instance;
	private static AppOpenManager appOpenManager;


	public static Context getAppContext() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		MobileAdsInstance.INSTANCE.init(this);
		appOpenManager = new AppOpenManager(this);
		Logger.addLogAdapter(new AndroidLogAdapter());
		PowerPreference.init(this);
		//FirebaseAnalytics.getInstance(this);

	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
		MultiDex.install(this);
	}

	public static Application getApplication() {
		return instance;
	}

}
