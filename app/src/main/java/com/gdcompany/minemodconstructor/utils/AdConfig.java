package com.gdcompany.minemodconstructor.utils;

public class AdConfig {
    public static final String AD_NATIVE_MAIN_SCREEN_RELEASE_ID = "ca-app-pub-2531835920111883/6938332920";
    public static final String AD_NATIVE_START_SCREEN_RELEASE_ID = "ca-app-pub-2531835920111883/4312169587";
    public static final String AD_APP_OPEN_RELEASE_ID = "ca-app-pub-2531835920111883/1415894578";
    public static final String AD_INTERSTITIAL_RELEASE_ID = "ca-app-pub-2531835920111883/4503741274";
    public static final String AD_REWARDED_RELEASE_ID = "ca-app-pub-2531835920111883/1877577939";
}
