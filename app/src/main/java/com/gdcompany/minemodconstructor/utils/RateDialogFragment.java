package com.gdcompany.minemodconstructor.utils;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.gdcompany.minemodconstructor.R;

public class RateDialogFragment extends DialogFragment {

    ImageView mStar1;
    ImageView mStar2;
    ImageView mStar3;
    ImageView mStar4;
    ImageView mStar5;
    ImageView mWolf;
    boolean mOpenGooglePlay = false;
    final int openGPRate = 4;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static RateDialogFragment getInstance() {
        return new RateDialogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.fragment_rate_dialog, container, false);
        mStar1 = view.findViewById(R.id.star1);
        if (mStar1 != null)
            mStar1.setOnClickListener((l) -> fillStars(1));

        mStar2 = view.findViewById(R.id.star2);
        if (mStar2 != null)
            mStar2.setOnClickListener((l) -> fillStars(2));

        mStar3 = view.findViewById(R.id.star3);
        if (mStar3 != null)
            mStar3.setOnClickListener((l) -> fillStars(3));

        mStar4 = view.findViewById(R.id.star4);
        if (mStar4 != null)
            mStar4.setOnClickListener((l) -> fillStars(4));

        mStar5 = view.findViewById(R.id.star5);
        if (mStar5 != null)
            mStar5.setOnClickListener((l) -> fillStars(5));

        mWolf = view.findViewById(R.id.ivWolf);

        AppCompatButton rate = view.findViewById(R.id.rateBtn);
        if (rate != null)
            rate.setOnClickListener((l) -> {
                if (getActivity() == null || getActivity().isFinishing())
                    return;
                if (mOpenGooglePlay) {
                    SocialManager.onAppRate(getActivity());
                } else {
                    new FeedbackDialogFragment().getInstance().onShow(getActivity().getSupportFragmentManager(), "Feedback");
                }
                dismiss();
            });

        ImageButton btnClose = view.findViewById(R.id.btnClose);
        if (btnClose != null)
            btnClose.setOnClickListener((v) -> dismiss());

        return view;
    }


    public void onShow(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        if (ft != null) {
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        }
    }

    void fillStars(int rate) {
        if (mStar1 != null)
            mStar1.setImageResource(R.drawable.star1_on);
        if (mStar2 != null)
            mStar2.setImageResource(R.drawable.star2_off);
        if (mStar3 != null)
            mStar3.setImageResource(R.drawable.star3_off);
        if (mStar4 != null)
            mStar4.setImageResource(R.drawable.star4_off);
        if (mStar5 != null)
            mStar5.setImageResource(R.drawable.star5_off);

        if (rate > 1 && mStar2 != null)
            mStar2.setImageResource(R.drawable.star2_on);
        if (rate > 2 && mStar3 != null)
            mStar3.setImageResource(R.drawable.star3_on);
        if (rate > 3 && mStar4 != null)
            mStar4.setImageResource(R.drawable.star4_on);
        if (rate > 4 && mStar5 != null)
            mStar5.setImageResource(R.drawable.star5_on);

        if (mWolf != null) {
            if (rate < 3)
                mWolf.setImageResource(R.drawable.wolf_cry);
            else if (rate == 3)
                mWolf.setImageResource(R.drawable.wolf_surprised);
            else if (rate > 3)
                mWolf.setImageResource(R.drawable.wolf_cool);
        }


        if (rate >= openGPRate)
            mOpenGooglePlay = true;
        else
            mOpenGooglePlay = false;
    }

}
