package com.gdcompany.minemodconstructor.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.gdcompany.minemodconstructor.R;

public class SocialManager {

    public static void onAppRate(Context context) {
        onOpenGooglePlay(context, context.getPackageName());
    }

    public static void onOpenGooglePlay(Context context, String packageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (ActivityNotFoundException anfe) {
            try {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)));
            } catch (ActivityNotFoundException anfe1){
                Toast.makeText(context,
                        context.getString(R.string.error_opening_store_text), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
