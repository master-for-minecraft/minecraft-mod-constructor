package com.gdcompany.minemodconstructor.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.fragment.components.ItemViewPagerAdapter;
import com.gdcompany.minemodconstructor.model.ImageContentItem;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;

import java.util.List;

public class MyViewSwitcher extends ViewSwitcher {

    private ItemViewPagerAdapter mItemViewPagerAdapter;

    public MyViewSwitcher(Context context) {
        super(context);
    }

    public MyViewSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setViewPagerAdapter(List<ImageContentItem> images, PageIndicatorView pageIndicatorView, FragmentActivity activity) {
        ViewPager viewPager = (ViewPager) this.getNextView();
        viewPager.setAdapter(null);
        mItemViewPagerAdapter = new ItemViewPagerAdapter(images, activity, new IImageLoadStatus() {
            @Override
            public void imageLoadSuccess() {
                pageIndicatorView.setVisibility(View.VISIBLE);
            }

            @Override
            public void imageLoadFailed() {
                pageIndicatorView.setVisibility(View.GONE);
            }
        });
        viewPager.setAdapter(mItemViewPagerAdapter);
        mItemViewPagerAdapter.setViewPager(viewPager);
        mItemViewPagerAdapter.onSwipePager();
        pageIndicatorView.setViewPager(viewPager);
        pageIndicatorView.setRadius(getResources().getDimension(R.dimen.page_indicator_radius));
        pageIndicatorView.setAnimationType(AnimationType.SLIDE);
        //pageIndicatorView.setVisibility(View.GONE);
        showNext();
    }

    public ItemViewPagerAdapter getItemViewPagerAdapter() {
        return mItemViewPagerAdapter;
    }
}
