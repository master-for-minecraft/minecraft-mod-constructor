package com.gdcompany.minemodconstructor.utils;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;

public class FeedbackDialogFragment  extends DialogFragment {

    EditText etFeedback;
    TextView tvFeedback;
    RadioButton rbVariant5;
    RadioButton rbVariant4;
    RadioButton rbVariant3;
    RadioButton rbVariant2;
    RadioButton rbVariant1;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.fragment_feedback_dialog, container, false);
        etFeedback = view.findViewById(R.id.etFeedbackText);
        tvFeedback = view.findViewById(R.id.feedbackTip);
        rbVariant5 = view.findViewById(R.id.rbVariant5);
        rbVariant5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    if(etFeedback != null)
                        etFeedback.setVisibility(View.VISIBLE);
                    if(tvFeedback != null)
                        tvFeedback.setVisibility(View.VISIBLE);
                }
                else{
                    if(etFeedback != null)
                        etFeedback.setVisibility(View.INVISIBLE);
                    if(tvFeedback != null)
                        tvFeedback.setVisibility(View.INVISIBLE);
                }

            }
        });

        rbVariant1 = view.findViewById(R.id.rbVariant1);
        rbVariant2 = view.findViewById(R.id.rbVariant2);
        rbVariant3 = view.findViewById(R.id.rbVariant3);
        rbVariant4 = view.findViewById(R.id.rbVariant4);
        Button btnSend = view.findViewById(R.id.btnSend);
        if(btnSend != null)
            btnSend.setOnClickListener((v)->{handleAnswer();});
        ImageButton btnClose = view.findViewById(R.id.btnClose);
        if (btnClose != null)
            btnClose.setOnClickListener((v) -> dismiss());
        return view;
    }

    public static FeedbackDialogFragment getInstance() {
        return new FeedbackDialogFragment();
    }


    public void onShow(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        if(ft != null) {
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        }
    }

    public void handleAnswer(){
        String subject = "";
        String message = "";
        if(rbVariant1 != null && rbVariant1.isChecked()) {
            subject = rbVariant1.getText().toString();
        }
        else if(rbVariant2 != null && rbVariant2.isChecked()) {
            subject = rbVariant2.getText().toString();
        }
        else if(rbVariant3 != null && rbVariant3.isChecked()) {
            subject = rbVariant3.getText().toString();
        }
        else if(rbVariant4 != null && rbVariant4.isChecked()) {
            subject = rbVariant4.getText().toString();
        }
        else if(rbVariant5 != null && rbVariant5.isChecked()){
            if(etFeedback == null || etFeedback.getText().toString().isEmpty())
                return;
            subject = rbVariant5.getText().toString();
            message = etFeedback.getText().toString();
        }

        if(getActivity() != null && !getActivity().isFinishing()) {
            dismiss();
            AppUtil.sendEmail(getActivity(), AppConfigSharedModel.SUPPORT_EMAIL, subject, message);
        }
    }
}
