package com.gdcompany.minemodconstructor.utils;

import com.gdcompany.minemodconstructor.fragment.components.ItemDownloadButton;

public interface IRewardedAdOutput {
    void rewardEarned(ItemDownloadButton item);
    void rewardFailed(String reason);
}
