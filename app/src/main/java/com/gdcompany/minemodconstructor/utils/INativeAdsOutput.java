package com.gdcompany.minemodconstructor.utils;

import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;

public interface INativeAdsOutput {
    void nativeAdsLoaded(NativeAd nativeAd);
    void nativeAdsFailed();
}
