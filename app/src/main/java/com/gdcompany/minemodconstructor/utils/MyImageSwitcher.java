package com.gdcompany.minemodconstructor.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageSwitcher;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gdcompany.minemodconstructor.R;
import com.google.firebase.storage.StorageReference;

public class MyImageSwitcher extends ImageSwitcher {

    public MyImageSwitcher(Context context) {
        super(context);
    }

    public MyImageSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setImageUrl(StorageReference storageReference, IImageLoadStatus callback) {
        CircularProgressDrawable drawable = new CircularProgressDrawable(getContext());
        drawable.setColorSchemeColors(Color.WHITE);
        drawable.setCenterRadius(60f);
        drawable.setStrokeWidth(8f);
        drawable.start();
        ImageView image = (ImageView) this.getNextView();
        GlideApp.with(this)
                .load(storageReference)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        callback.imageLoadFailed();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        callback.imageLoadSuccess();
                        return false;
                    }
                })
                .placeholder(drawable)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image);
        showNext();
    }
}
