package com.gdcompany.minemodconstructor.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.gdcompany.minemodconstructor.R;

public class AppUtil {
    public static void sendEmail(Context ctx, String address, String subject, String message){
        Intent mailIntent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?subject=" + subject + "&body=" + message + "&to=" + address);
        mailIntent.setData(data);

        try {
            ctx.startActivity(Intent.createChooser(mailIntent, null));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ctx,
                    ctx.getString(R.string.no_email_client_text), Toast.LENGTH_SHORT).show();
        }

    }
}
