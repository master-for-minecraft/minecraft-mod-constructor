package com.gdcompany.minemodconstructor.utils;

public interface IImageLoadStatus {
    void imageLoadSuccess();
    void imageLoadFailed();
}
