package com.gdcompany.minemodconstructor.utils;

import com.orhanobut.logger.Logger;

public final class LoggerManager {

    private static final boolean DEBUG_NAVIGATION = true;
    public static final boolean DEBUG_NETWORK = true;
    public static final boolean RELEASE_NETWORK = true;

    public static void printNavigationInfo(String info) {
        if (DEBUG_NAVIGATION) {
            Logger.i(info);
        }
    }
}
