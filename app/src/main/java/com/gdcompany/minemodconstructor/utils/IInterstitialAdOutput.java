package com.gdcompany.minemodconstructor.utils;

import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.nativead.NativeAd;

public interface IInterstitialAdOutput {
    void interstitialAdLoaded(InterstitialAd interstitialAd);
    void interstitialAdFailed();
}
