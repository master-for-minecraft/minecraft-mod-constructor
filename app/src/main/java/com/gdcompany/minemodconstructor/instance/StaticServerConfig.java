package com.gdcompany.minemodconstructor.instance;

import androidx.annotation.Keep;

@Keep
public final class StaticServerConfig {

    public static final String STATIC_SERVER_BASE_URL = "https://mod-constructor.planetcommander.ru/poppy/";
}
