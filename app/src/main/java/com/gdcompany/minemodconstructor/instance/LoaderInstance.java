package com.gdcompany.minemodconstructor.instance;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.widget.ContentLoadingProgressBar;

import com.bumptech.glide.Glide;

public enum LoaderInstance {

    INSTANCE_LOADER;

    private ImageView loadingView;
    private TextView loadingText;

    public void setContentLoader(ImageView loadingView, TextView loadingText) {
        this.loadingView = loadingView;
        this.loadingText = loadingText;
    }

    public void show() {
        Glide.with(loadingView.getContext()).load("file:///android_asset/loading_animation.gif").into(loadingView);
        loadingView.setVisibility(View.VISIBLE);
        loadingText.setVisibility(View.VISIBLE);
    }

    public void hide() {
        loadingView.setVisibility(View.GONE);
        loadingText.setVisibility(View.GONE);
    }
}
