package com.gdcompany.minemodconstructor.instance;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;

import com.gdcompany.minemodconstructor.BuildConfig;
import com.gdcompany.minemodconstructor.fragment.components.ItemDownloadButton;
import com.gdcompany.minemodconstructor.utils.AdConfig;
import com.gdcompany.minemodconstructor.utils.IInterstitialAdOutput;
import com.gdcompany.minemodconstructor.utils.INativeAdsOutput;
import com.gdcompany.minemodconstructor.utils.IRewardedAdOutput;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collections;

public enum MobileAdsInstance {

    INSTANCE;

    public boolean mobileAdsInitialised = false;
    public static String AD_NATIVE_MAIN_SCREEN_TEST_ID = "ca-app-pub-3940256099942544/2247696110";
    public static String AD_INTERSTITIAL_TEST_ID = "ca-app-pub-3940256099942544/1033173712";
    public static String AD_REWARDED_TEST_ID = "ca-app-pub-3940256099942544/5224354917";
    private InterstitialAd mInterstitialAd;
    private RewardedAd mRewardedAd;
    public final static long AD_COOLDOWN = 5000;
    private boolean isLoadingInterstitialAd = false;

    MobileAdsInstance() {

    }

    public void init(Context context) {
        MobileAds.initialize(context, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(@NotNull InitializationStatus initializationStatus) {
                mobileAdsInitialised = true;
            }
        });
    }

    public void loadInterstitialAd(Activity activity) {
        if (mInterstitialAd == null && !isLoadingInterstitialAd) {
            AdRequest adRequest = new AdRequest.Builder().build();
            isLoadingInterstitialAd = true;
            InterstitialAd.load(activity, (BuildConfig.DEBUG) ? AD_INTERSTITIAL_TEST_ID : AdConfig.AD_INTERSTITIAL_RELEASE_ID, adRequest,
                    new InterstitialAdLoadCallback() {
                        @Override
                        public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                            mInterstitialAd = interstitialAd;
                            isLoadingInterstitialAd = false;
                            //mInterstitialAd.show(activity);
                        }

                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            mInterstitialAd = null;
                            isLoadingInterstitialAd = false;
                        }
                    });
        }
    }

    public void loadInterstitialAd(Activity activity, IInterstitialAdOutput callback) {
        if (mInterstitialAd == null && !isLoadingInterstitialAd) {
            AdRequest adRequest = new AdRequest.Builder().build();
            isLoadingInterstitialAd = true;
            InterstitialAd.load(activity, (BuildConfig.DEBUG) ? AD_INTERSTITIAL_TEST_ID : AdConfig.AD_INTERSTITIAL_RELEASE_ID, adRequest,
                    new InterstitialAdLoadCallback() {
                        @Override
                        public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                            mInterstitialAd = interstitialAd;
                            isLoadingInterstitialAd = false;
                            callback.interstitialAdLoaded(interstitialAd);
                        }

                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            mInterstitialAd = null;
                            isLoadingInterstitialAd = false;
                            callback.interstitialAdFailed();
                        }
                    });
        } else {
            callback.interstitialAdFailed();
        }
    }

    public void showInterstitialAd(Activity activity) {
        if (mInterstitialAd != null && activity!=null) {
            mInterstitialAd.show(activity);
            mInterstitialAd = null;
        }
        loadInterstitialAd(activity);
    }

    public void loadRewardedAd(Activity activity, Context context, ItemDownloadButton item, IRewardedAdOutput callback) {
        AdRequest adRequest = new AdRequest.Builder().build();
        RewardedAd.load(context, (BuildConfig.DEBUG) ? AD_REWARDED_TEST_ID : AdConfig.AD_REWARDED_RELEASE_ID,
                adRequest, new RewardedAdLoadCallback() {
                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        mRewardedAd = null;
                    }

                    @Override
                    public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                        mRewardedAd = rewardedAd;
                        mRewardedAd.show(activity, new OnUserEarnedRewardListener() {
                            @Override
                            public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                                callback.rewardEarned(item);
                            }
                        });
                        mRewardedAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                            @Override
                            public void onAdShowedFullScreenContent() {
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NotNull AdError adError) {
                                callback.rewardFailed("");
                            }

                            @Override
                            public void onAdDismissedFullScreenContent() {
                                mRewardedAd = null;
                                callback.rewardFailed("");
                            }
                        });
                    }
                });
    }

    public void loadMainScreenNativeAd(Context context, INativeAdsOutput callback, String id) {
        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(true)
                .build();
        AdLoader adLoader = new AdLoader.Builder(context, (BuildConfig.DEBUG) ? id : AdConfig.AD_NATIVE_MAIN_SCREEN_RELEASE_ID)
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        if(nativeAd!=null) {
                            callback.nativeAdsLoaded(nativeAd);
                        } else {
                            callback.nativeAdsFailed();
                        }
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        callback.nativeAdsFailed();
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        .setVideoOptions(videoOptions).build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    public void loadStartScreenNativeAd(Context context, INativeAdsOutput callback, String id) {
        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(true)
                .build();
        AdLoader adLoader = new AdLoader.Builder(context, (BuildConfig.DEBUG) ? id : AdConfig.AD_NATIVE_START_SCREEN_RELEASE_ID)
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        if (nativeAd != null) {
                            callback.nativeAdsLoaded(nativeAd);
                        } else {
                            callback.nativeAdsFailed();
                        }
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        callback.nativeAdsFailed();
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        .setVideoOptions(videoOptions).build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }
}
