package com.gdcompany.minemodconstructor.instance;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;

import com.gdcompany.minemodconstructor.model.ModConfigModel;
import com.gdcompany.minemodconstructor.model.TryOtherModModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;
@Keep
public enum FirebaseRemoteConfigInstance {

    INSTANCE;

    private static final String CONFIG_NAME = "PoppyMODConfig";
    private static final String TRY_OTHER_CONFIG_NAME = "TryOtherConfig";

    private FirebaseRemoteConfig firebaseRemoteConfig;

    FirebaseRemoteConfigInstance() {
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(30)
                .build();
        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);
    }

    public interface IFirebaseRemoteConfigOutput {
        void configFetchSuccess(ModConfigModel modConfigModel, TryOtherModModel tryOtherModModel);
        void configFetchFailed();
    }

    public void fetchConfig(IFirebaseRemoteConfigOutput resultCallback) {
        firebaseRemoteConfig.fetchAndActivate().addOnCompleteListener(new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                if (task != null) {
                    if (task.isSuccessful()) {
                        if(firebaseRemoteConfig!=null) {
                            String modConfigJson = firebaseRemoteConfig.getString(CONFIG_NAME);
                            String tryOtherModJson = firebaseRemoteConfig.getString(TRY_OTHER_CONFIG_NAME);
                            if(!modConfigJson.isEmpty() && !tryOtherModJson.isEmpty()) {
                                ModConfigModel modConfigModel = parseModConfigToModel(modConfigJson);
                                TryOtherModModel tryOtherModModel = parseTryOtherModConfigToModel(tryOtherModJson);
                                resultCallback.configFetchSuccess(modConfigModel, tryOtherModModel);
                            } else {
                                resultCallback.configFetchFailed();
                            }
                        } else {
                            resultCallback.configFetchFailed();
                        }
                    } else {
                        resultCallback.configFetchFailed();
                    }
                } else {
                    resultCallback.configFetchFailed();
                }
            }
        });
    }

    private ModConfigModel parseModConfigToModel(String modConfigJson) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ModConfigModel> jsonAdapter = moshi.adapter(ModConfigModel.class);
        ModConfigModel modConfigModel = null;
        try {
            modConfigModel = jsonAdapter.fromJson(modConfigJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return modConfigModel;
    }

    private TryOtherModModel parseTryOtherModConfigToModel(String configJson) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<TryOtherModModel> jsonAdapter = moshi.adapter(TryOtherModModel.class);
        TryOtherModModel tryOtherModModel = null;
        try {
            tryOtherModModel = jsonAdapter.fromJson(configJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tryOtherModModel;
    }

}
