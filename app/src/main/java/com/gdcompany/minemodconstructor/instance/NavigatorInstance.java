package com.gdcompany.minemodconstructor.instance;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.navigation.NavController;

public enum NavigatorInstance {
	INSTANCE;

	private NavController navController;

	public NavController getNavController() {
		return navController;
	}

	public void setNavController(NavController navController) {
		this.navController = navController;
	}

	public static void openBrowserIntent(String url, Context context) {
		Intent browserIntent =
				new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		context.startActivity(browserIntent);
	}

	public static void openDialIntent(String phone, Context context) {
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
		context.startActivity(intent);
	}

	public static void openEmailIntent(String email, Context context) {
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
				"mailto", "email", null));
		context.startActivity(emailIntent);
	}
}
