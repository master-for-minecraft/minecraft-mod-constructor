package com.gdcompany.minemodconstructor.model;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.gdcompany.minemodconstructor.MainApplication;
import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.fragment.components.ActionButtonType;
import com.gdcompany.minemodconstructor.fragment.components.InstallStep;
import com.gdcompany.minemodconstructor.fragment.components.ItemDownloadButton;
import com.gdcompany.minemodconstructor.fragment.components.ItemInstallButton;
import com.gdcompany.minemodconstructor.utils.FileSize;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.preference.MapStructure;
import com.preference.PowerPreference;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
@Keep
public class AppConfigSharedModel extends ViewModel {

    public static final String SUPPORT_EMAIL = "m4mreports@masterforminecraft.com";

    private ModConfigModel modConfigModel;

    private TryOtherModModel tryOtherModModel;

    private InstallStep currentStep = InstallStep.STEP_INFO_ONE;

    public final static int MAX_STEP = 7;

    public final static int MIN_STEP = 1;

    private final static int FAKE_FILE_COUNT = 3;

    private List<ItemDownloadButton> downloadList;

    private List<ItemInstallButton> installList;

    private List<ImageContentItem> infoOneImages;

    private List<ImageContentItem> infoTwoImages;

    private List<ImageContentItem> infoThreeImages;

    private HashMap<String, File> modFiles;

    private HashMap<Integer, FakeFileCache> fakeFiles;

    private NativeAd startScreenNativeAd;

    private Locale currentLocale;

    private String currentLocaleString;

    private HashMap<String, String> nameLocaleMap;

    private HashMap<String, String> textInfoOneLocaleMap;

    private HashMap<String, String> textInfoTwoLocaleMap;

    private HashMap<String, String> textInfoThreeLocaleMap;

    public HashMap<String, String> getTextInfoThreeLocaleMap() {
        return textInfoThreeLocaleMap;
    }

    public void setTextInfoThreeLocaleMap(HashMap<String, String> textInfoThreeLocaleMap) {
        this.textInfoThreeLocaleMap = textInfoThreeLocaleMap;
    }

    public HashMap<String, String> getTextInfoTwoLocaleMap() {
        return textInfoTwoLocaleMap;
    }

    public void setTextInfoTwoLocaleMap(HashMap<String, String> textInfoTwoLocaleMap) {
        this.textInfoTwoLocaleMap = textInfoTwoLocaleMap;
    }

    public HashMap<String, String> getTextInfoOneLocaleMap() {
        return textInfoOneLocaleMap;
    }

    public void setTextInfoOneLocaleMap(HashMap<String, String> textInfoOneLocaleMap) {
        this.textInfoOneLocaleMap = textInfoOneLocaleMap;
    }

    public HashMap<String, String> getNameLocaleMap() {
        return nameLocaleMap;
    }

    public void setNameLocaleMap(HashMap<String, String> nameLocaleMap) {
        this.nameLocaleMap = nameLocaleMap;
    }

    public void setCurrentLocale(Locale currentLocale) {
        this.currentLocale = currentLocale;
    }

    public String getCurrentLocaleString() {
        return currentLocaleString;
    }

    private void generateNameLocaleMap() {
        if(modConfigModel!=null) {
            if (modConfigModel.getNameLocale() != null) {
                List<NameLocaleItem> nameLocaleItems = modConfigModel.getNameLocale();
                HashMap<String, String> nameLocaleMap = new HashMap<>();
                for (NameLocaleItem localeItem : nameLocaleItems) {
                    if (!nameLocaleMap.containsKey(localeItem.getLocale())) {
                        nameLocaleMap.put(localeItem.getLocale(), localeItem.getContent());
                    }
                }
                setNameLocaleMap(nameLocaleMap);
            }
        }
    }

    private void generateTextInfoOneLocaleMap() {
        if(modConfigModel.getTextInfoOneLocale()!=null) {
            List<TextInfoLocaleItem> textInfoOneLocaleItems = modConfigModel.getTextInfoOneLocale();
            HashMap<String, String> textInfoOneLocaleMap = new HashMap<>();
            for(TextInfoLocaleItem localeItem: textInfoOneLocaleItems) {
                if(!textInfoOneLocaleMap.containsKey(localeItem.getLocale())) {
                    textInfoOneLocaleMap.put(localeItem.getLocale(),localeItem.getContent());
                }
            }
            setTextInfoOneLocaleMap(textInfoOneLocaleMap);
        }
    }

    private void generateTextInfoTwoLocaleMap() {
        if(modConfigModel.getTextInfoTwoLocale()!=null) {
            List<TextInfoLocaleItem> textInfoTwoLocaleItems = modConfigModel.getTextInfoTwoLocale();
            HashMap<String, String> textInfoTwoLocaleMap = new HashMap<>();
            for(TextInfoLocaleItem localeItem: textInfoTwoLocaleItems) {
                if(!textInfoTwoLocaleMap.containsKey(localeItem.getLocale())) {
                    textInfoTwoLocaleMap.put(localeItem.getLocale(),localeItem.getContent());
                }
            }
            setTextInfoTwoLocaleMap(textInfoTwoLocaleMap);
        }
    }

    private void generateTextInfoThreeLocaleMap() {
        if(modConfigModel.getTextInfoThreeLocale()!=null) {
            List<TextInfoLocaleItem> textInfoThreeLocaleItems = modConfigModel.getTextInfoThreeLocale();
            HashMap<String, String> textInfoThreeLocaleMap = new HashMap<>();
            for(TextInfoLocaleItem localeItem: textInfoThreeLocaleItems) {
                if(!textInfoThreeLocaleMap.containsKey(localeItem.getLocale())) {
                    textInfoThreeLocaleMap.put(localeItem.getLocale(),localeItem.getContent());
                }
            }
            setTextInfoThreeLocaleMap(textInfoThreeLocaleMap);
        }
    }

    private void generateTextInfoLocaleMaps() {
        generateTextInfoOneLocaleMap();
        generateTextInfoTwoLocaleMap();
        generateTextInfoThreeLocaleMap();
    }

    public void setCurrentLocaleString(String currentLocaleString) {
        this.currentLocaleString = currentLocaleString;
    }

    public NativeAd getStartScreenNativeAd() {
        return startScreenNativeAd;
    }

    public void setStartScreenNativeAd(NativeAd startScreenNativeAd) {
        this.startScreenNativeAd = startScreenNativeAd;
    }

    public AppConfigSharedModel() {
        modFiles = new HashMap<>();
        fakeFiles = new HashMap<>();
        installList = new ArrayList<>();
        infoOneImages = new ArrayList<>();
        infoTwoImages = new ArrayList<>();
        infoThreeImages = new ArrayList<>();
        textInfoOneLocaleMap = new HashMap<>();
        textInfoTwoLocaleMap = new HashMap<>();
        textInfoThreeLocaleMap = new HashMap<>();
        nameLocaleMap = new HashMap<>();
    }

    public StorageReference getModImageStorageReferenceByIndex(int index) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(getModConfigModel().getFirebaseStorageLink()).child(getModConfigModel().getFirebaseStorageFolder()).child("images/");
        return storageRef;//.child(getModConfigModel().getImages().get(index));
    }

    public void addModFile(File modFile) {
        modFiles.put(modFile.getAbsolutePath(), modFile);
        PowerPreference.getDefaultFile().putMap("MOD_FILES_MAP", modFiles);
    }

    public void addFakeFile(int position, ItemDownloadButton item) {
        if(item!=null) {
            fakeFiles.put(position,new FakeFileCache(item.isDownloadCompleted(), item.isRewardEarned()));
            PowerPreference.getDefaultFile().putMap("FAKE_FILES_MAP", fakeFiles);
        }
    }

    public void removeModFile(File modFile) {
        modFiles.remove(modFile.getAbsolutePath());
        //PowerPreference.getDefaultFile().putMap("MOD_FILES_MAP", modFiles);
    }

    public String getModNameText() {
        HashMap<String, String> nameLocaleMap = getNameLocaleMap();
        String currentLocale = getCurrentLocaleString();
        String modNameText = "";
        if(nameLocaleMap!=null) {
            if (nameLocaleMap.containsKey(currentLocale)) {
                modNameText = nameLocaleMap.get(currentLocale);
            } else {
                modNameText = nameLocaleMap.get("en_EN");
            }
        } else {

        }
        return modNameText;
    }

    public List<ItemDownloadButton> getDownloadList() {
        return downloadList;
    }

    public HashMap<String, File> getModFilesFromPrefs() {
        return PowerPreference.getDefaultFile().getMap("MOD_FILES_MAP", MapStructure.create(HashMap.class, String.class, File.class));
    }

    public HashMap<Integer, FakeFileCache> getFakeFilesFromPrefs() {
        return PowerPreference.getDefaultFile().getMap("FAKE_FILES_MAP", MapStructure.create(HashMap.class, Integer.class, FakeFileCache.class));
    }

    public List<ItemInstallButton> getInstallList() {
        List<ItemInstallButton> installList = new ArrayList<>();
        if (!modFiles.isEmpty()) {
            for (String key : modFiles.keySet()) {
                ItemInstallButton item = new ItemInstallButton(ActionButtonType.INSTALL, modFiles.get(key), key, MainApplication.getAppContext().getString(R.string.install_button_text));
                installList.add(item);
            }
        }
        installList.add(new ItemInstallButton(ActionButtonType.RATE_US, null, "", MainApplication.getAppContext().getString(R.string.rate_us_text)));
        return installList;
    }

    public InstallStep getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(InstallStep currentStep) {
        this.currentStep = currentStep;
    }

    public ModConfigModel getModConfigModel() {
        return modConfigModel;
    }

    public List<ImageContentItem> getInfoOneImages() {
        return infoOneImages;
    }

    public List<ImageContentItem> getInfoTwoImages() {
        return infoTwoImages;
    }

    public List<ImageContentItem> getInfoThreeImages() {
        return infoThreeImages;
    }

    public void setInfoOneImages(List<ImageContentItem> infoOneImages) {
        this.infoOneImages = infoOneImages;
    }

    public void setInfoTwoImages(List<ImageContentItem> infoTwoImages) {
        this.infoTwoImages = infoTwoImages;
    }

    public void setInfoThreeImages(List<ImageContentItem> infoThreeImages) {
        this.infoThreeImages = infoThreeImages;
    }

    public void setModConfigModel(ModConfigModel modConfigModel) {
        this.modConfigModel = modConfigModel;
        generateNameLocaleMap();
        generateTextInfoLocaleMaps();
        setAllModDownloadStorageReference();
        setAllImageContentStorageReference();
    }

    public void setTryOtherModModel(TryOtherModModel tryOtherModModel) {
        this.tryOtherModModel = tryOtherModModel;
        setAllOtherModImageStorageReference();
    }

    public TryOtherModModel getTryOtherModModel() {
        return tryOtherModModel;
    }

    public void setDownloadList(List<ItemDownloadButton> downloadList) {
        this.downloadList = downloadList;
    }

    private void setAllOtherModImageStorageReference() {
        if (tryOtherModModel != null) {
            if (tryOtherModModel.getOtherMods() != null) {
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReferenceFromUrl(getModConfigModel().getFirebaseStorageLink()).child(tryOtherModModel.getFirebaseStorageFolder());
                for (TryOtherModModelItem item : tryOtherModModel.getOtherMods()) {
                    item.setStorageReference(storageRef.child(item.getImage()));
                }
            }
        }
    }

    private void setAllImageContentStorageReference() {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        List<ImageContentItem> infoOneImages = new ArrayList<>();
        List<ImageContentItem> infoTwoImages = new ArrayList<>();
        List<ImageContentItem> infoThreeImages = new ArrayList<>();
        if (modConfigModel != null && modConfigModel.getImageContent() != null) {
            if (modConfigModel.getImageContent().getInfoOne() != null) {
                for (String imageString : modConfigModel.getImageContent().getInfoOne()) {
                    StorageReference storageRef = storage.getReferenceFromUrl(getModConfigModel().getFirebaseStorageLink()).child(getModConfigModel().getFirebaseStorageFolder()).child("images/");
                    infoOneImages.add(new ImageContentItem(storageRef.child(imageString)));
                }
                setInfoOneImages(infoOneImages);
            }
            if (modConfigModel.getImageContent().getInfoTwo() != null) {
                for (String imageString : modConfigModel.getImageContent().getInfoTwo()) {
                    StorageReference storageRef = storage.getReferenceFromUrl(getModConfigModel().getFirebaseStorageLink()).child(getModConfigModel().getFirebaseStorageFolder()).child("images/");
                    infoTwoImages.add(new ImageContentItem(storageRef.child(imageString)));
                }
                setInfoTwoImages(infoTwoImages);
            }
            if (modConfigModel.getImageContent().getInfoThree() != null) {
                for (String imageString : modConfigModel.getImageContent().getInfoThree()) {
                    StorageReference storageRef = storage.getReferenceFromUrl(getModConfigModel().getFirebaseStorageLink()).child(getModConfigModel().getFirebaseStorageFolder()).child("images/");
                    infoThreeImages.add(new ImageContentItem(storageRef.child(imageString)));
                }
                setInfoThreeImages(infoThreeImages);
            }
        }
    }

    private void setAllModDownloadStorageReference() {
        List<ItemDownloadButton> downloadList = new ArrayList<>();
        HashMap<String, File> modFilesCache = getModFilesFromPrefs();
        HashMap<Integer, FakeFileCache> fakeFilesCache = getFakeFilesFromPrefs();
        if(fakeFilesCache!=null) {
            fakeFiles = getFakeFilesFromPrefs();
        }
        if (modConfigModel != null) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl(getModConfigModel().getFirebaseStorageLink()).child(getModConfigModel().getFirebaseStorageFolder()).child("files/");
            if (modConfigModel.getFiles() != null) {
                for (String item : modConfigModel.getFiles()) {
                    final String[] fileSize = {""};
                    storageRef.child(item).getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                        @Override
                        public void onSuccess(StorageMetadata storageMetadata) {
                            String fileName = Objects.requireNonNull(storageMetadata.getName()).split("\\.")[0];
                            String fileExt = storageMetadata.getName().split("\\.")[1];
                            fileSize[0] = FileSize.convertToStringRepresentation(storageMetadata.getSizeBytes());
                            for (int i = 0; i < FAKE_FILE_COUNT; i++) {
                                ItemDownloadButton itemDownloadButton = new ItemDownloadButton(fileName, fileExt, fileSize[0], storageMetadata.getSizeBytes(), storageRef.child(item), true);
                                if(fakeFiles!=null && !fakeFiles.isEmpty()) {
                                    if(fakeFiles.containsKey(i) && fakeFiles.get(i)!=null) {
                                        itemDownloadButton.setDownloadCompleted(fakeFiles.get(i).isDownloadComplete());
                                        itemDownloadButton.setRewardEarned(fakeFiles.get(i).isRewardEarned());
                                    }
                                }
                                downloadList.add(itemDownloadButton);
                            }
                            downloadList.add(new ItemDownloadButton(fileName, fileExt, fileSize[0], storageMetadata.getSizeBytes(), storageRef.child(item), false));
                            for (ItemDownloadButton downloadItem : downloadList) {
                                if(modFilesCache!=null && !modFilesCache.isEmpty()) {
                                    for(File cachedFile : modFilesCache.values()) {
                                        if (cachedFile.getName().equalsIgnoreCase(downloadItem.getFullName()) && cachedFile.length() == downloadItem.getFileSizeLong() && !downloadItem.isFake()) {
                                            downloadItem.setDownloadCompleted(true);
                                            downloadItem.setRewardEarned(true);
                                            addModFile(cachedFile);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                        }
                    });
                }
            }
        }
        setDownloadList(downloadList);
    }

    public void nextStep() {
        int currentStepId = getCurrentStep().getStepId();
        if (currentStepId < MAX_STEP) {
            currentStepId++;
            setCurrentStep(InstallStep.fromStepId(currentStepId));
        } else {
            setCurrentStep(InstallStep.STEP_TRY_OTHER_MOD);
        }
    }

    public void prevStep() {
        int currentStepId = getCurrentStep().getStepId();
        if (currentStepId > MIN_STEP) {
            currentStepId--;
            setCurrentStep(InstallStep.fromStepId(currentStepId));
        } else {
            setCurrentStep(InstallStep.STEP_INFO_ONE);
        }
    }

    public String getInfoStepText() {
        InstallStep currentStep = getCurrentStep();
        String textToReturn = "";
            switch (currentStep) {
                case STEP_INFO_ONE:
                    textToReturn = getTextInfoOneLocaleString();
                    break;
                case STEP_INFO_TWO:
                    textToReturn = getTextInfoTwoLocaleString();
                    break;
                case STEP_INFO_THREE:
                    textToReturn = getTextInfoThreeLocaleString();
                    break;
            }
        return textToReturn;
    }

    private String getTextInfoOneLocaleString() {
        HashMap<String, String> textInfoOneLocaleMap = getTextInfoOneLocaleMap();
        String currentLocale = getCurrentLocaleString();
        String localeText;
        if(textInfoOneLocaleMap.containsKey(currentLocale)) {
            localeText = textInfoOneLocaleMap.get(currentLocale);
        } else {
            localeText = textInfoOneLocaleMap.get("en_EN");
        }
        return localeText;
    }

    private String getTextInfoTwoLocaleString() {
        HashMap<String, String> textInfoTwoLocaleMap = getTextInfoTwoLocaleMap();
        String currentLocale = getCurrentLocaleString();
        String localeText;
        if(textInfoTwoLocaleMap.containsKey(currentLocale)) {
            localeText = textInfoTwoLocaleMap.get(currentLocale);
        } else {
            localeText = textInfoTwoLocaleMap.get("en_EN");
        }
        return localeText;
    }

    private String getTextInfoThreeLocaleString() {
        HashMap<String, String> textInfoThreeLocaleMap = getTextInfoThreeLocaleMap();
        String currentLocale = getCurrentLocaleString();
        String localeText;
        if(textInfoThreeLocaleMap.containsKey(currentLocale)) {
            localeText = textInfoThreeLocaleMap.get(currentLocale);
        } else {
            localeText = textInfoThreeLocaleMap.get("en_EN");
        }
        return localeText;
    }
}
