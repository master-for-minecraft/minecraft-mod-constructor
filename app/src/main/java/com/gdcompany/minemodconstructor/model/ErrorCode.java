package com.gdcompany.minemodconstructor.model;

public enum ErrorCode {
	UNAUTHORIZED(401, "Пользователь не авторизован"),
	EMPTY_LOGIN_OR_PASSWORD(204, "Заполните поля «Логин» и «Пароль»"),
	INCORRECT_LOGIN_OR_PASSWORD(203, "Неверный логин или пароль"),
	UNKNOWN_ERROR(0, "Неизвестный код ошибки");

	private final int code;
	private final String message;

	ErrorCode(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public static String getMessageByCode(int code) {
		for (ErrorCode value : values()) {
			if (value.code == code) {
				return value.message;
			}
		}

		return ErrorCode.UNKNOWN_ERROR.message;
	}
}
