package com.gdcompany.minemodconstructor.model;

import androidx.annotation.Keep;

import java.util.List;
import com.squareup.moshi.Json;
@Keep
public class ImageContent {

	@Json(name = "infoThree")
	private List<String> infoThree;

	@Json(name = "infoTwo")
	private List<String> infoTwo;

	@Json(name = "infoOne")
	private List<String> infoOne;

	public List<String> getInfoThree(){
		return infoThree;
	}

	public List<String> getInfoTwo(){
		return infoTwo;
	}

	public List<String> getInfoOne(){
		return infoOne;
	}
}