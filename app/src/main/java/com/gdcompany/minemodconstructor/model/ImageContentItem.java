package com.gdcompany.minemodconstructor.model;

import androidx.annotation.Keep;

import com.google.firebase.storage.StorageReference;
@Keep
public class ImageContentItem {

    private transient StorageReference storageReference;

    public StorageReference getStorageReference() {
        return storageReference;
    }

    public ImageContentItem(StorageReference storageReference) {
        this.storageReference = storageReference;
    }

}
