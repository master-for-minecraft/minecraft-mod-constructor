package com.gdcompany.minemodconstructor.model;

import androidx.annotation.Keep;

import java.util.List;
import com.squareup.moshi.Json;
@Keep
public class ModConfigModel{

	@Json(name = "images")
	private ImageContent imageContent;

	@Json(name = "files")
	private List<String> files;

	@Json(name = "firebaseStorageFolder")
	private String firebaseStorageFolder;

	@Json(name = "firebaseStorageLink")
	private String firebaseStorageLink;

	@Json(name = "nameLocale")
	private List<NameLocaleItem> nameLocale;

	@Json(name = "textInfoOneLocale")
	private List<TextInfoLocaleItem> textInfoOneLocale;

	@Json(name = "textInfoTwoLocale")
	private List<TextInfoLocaleItem> textInfoTwoLocale;

	@Json(name = "textInfoThreeLocale")
	private List<TextInfoLocaleItem> textInfoThreeLocale;

	public List<TextInfoLocaleItem> getTextInfoOneLocale() {
		return textInfoOneLocale;
	}

	public List<TextInfoLocaleItem> getTextInfoThreeLocale() {
		return textInfoThreeLocale;
	}

	public List<TextInfoLocaleItem> getTextInfoTwoLocale() {
		return textInfoTwoLocale;
	}

	public List<NameLocaleItem> getNameLocale() {
		return nameLocale;
	}

	public List<String> getFiles() {
		return files;
	}

	public ImageContent getImageContent(){
		return imageContent;
	}

	public String getFirebaseStorageFolder(){
		return firebaseStorageFolder;
	}

	public String getFirebaseStorageLink(){
		return firebaseStorageLink;
	}
}