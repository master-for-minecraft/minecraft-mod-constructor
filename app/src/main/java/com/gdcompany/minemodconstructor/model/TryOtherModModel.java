package com.gdcompany.minemodconstructor.model;

import androidx.annotation.Keep;

import java.util.ArrayList;
import java.util.List;
import com.squareup.moshi.Json;
@Keep
public class TryOtherModModel {

	@Json(name = "otherMods")
	private List<TryOtherModModelItem> otherMods;

	@Json(name = "firebaseStorageFolder")
	private String firebaseStorageFolder;

	@Json(name = "title")
	private String title;

	public List<TryOtherModModelItem> getOtherMods(){
		if(otherMods!=null) {
			return otherMods;
		} else {
			return new ArrayList<TryOtherModModelItem>();
		}
	}

	public String getFirebaseStorageFolder(){
		return firebaseStorageFolder;
	}

	public String getTitle(){
		return title;
	}
}