package com.gdcompany.minemodconstructor.model;

import androidx.annotation.Keep;

@Keep
public class FakeFileCache {

    private boolean isRewardEarned = false;
    private boolean isDownloadComplete = false;

    public FakeFileCache(boolean isDownloadComplete, boolean isRewardEarned) {
        this.isDownloadComplete = isDownloadComplete;
        this.isRewardEarned = isRewardEarned;
    }

    public boolean isRewardEarned() {
        return isRewardEarned;
    }

    public boolean isDownloadComplete() {
        return isDownloadComplete;
    }
}
