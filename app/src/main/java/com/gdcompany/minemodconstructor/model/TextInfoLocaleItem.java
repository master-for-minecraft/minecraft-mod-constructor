package com.gdcompany.minemodconstructor.model;

import androidx.annotation.Keep;

import com.squareup.moshi.Json;
@Keep
public class TextInfoLocaleItem {
    @Json(name = "locale")
    private String locale;

    @Json(name = "content")
    private String content;

    public String getLocale(){
        return locale;
    }

    public String getContent(){
        return content;
    }
}
