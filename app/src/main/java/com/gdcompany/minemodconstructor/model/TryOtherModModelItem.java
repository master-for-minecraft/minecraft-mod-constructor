package com.gdcompany.minemodconstructor.model;

import androidx.annotation.Keep;

import com.google.firebase.storage.StorageReference;
import com.squareup.moshi.Json;
@Keep
public class TryOtherModModelItem {

	@Json(name = "image")
	private String image;

	@Json(name = "link")
	private String link;

	@Json(name = "name")
	private String name;

	private transient StorageReference storageReference;

	public StorageReference getStorageReference() {
		return storageReference;
	}

	public void setStorageReference(StorageReference storageReference) {
		this.storageReference = storageReference;
	}

	public String getImage(){
		return image;
	}

	public String getLink(){
		return link;
	}

	public String getName() {
		return name;
	}
}