package com.gdcompany.minemodconstructor.fragment.screens.start;

import com.gdcompany.minemodconstructor.basic.BasicFragmentInteractor;

interface IStartFragmentInteractorOutput {
}

public class StartFragmentInteractor extends BasicFragmentInteractor<StartFragmentPresenter> {

    public StartFragmentInteractor(StartFragmentPresenter presenter) {
        super(presenter);
    }
}