package com.gdcompany.minemodconstructor.fragment.screens.info;

import com.gdcompany.minemodconstructor.basic.BasicFragmentModel;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;

public class InformationFragmentModel extends BasicFragmentModel<AppConfigSharedModel> {

    public InformationFragmentModel() {

    }

    public InformationFragmentModel(AppConfigSharedModel viewModel) {
        super(viewModel);
    }

}