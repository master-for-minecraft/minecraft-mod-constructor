package com.gdcompany.minemodconstructor.fragment.screens.info;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import android.view.ViewGroup.LayoutParams;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.basic.BasicFragment;
import com.gdcompany.minemodconstructor.basic.ToastType;
import com.gdcompany.minemodconstructor.fragment.components.ActionButtonType;
import com.gdcompany.minemodconstructor.fragment.components.DownloadButtonRecyclerViewAdapter;
import com.gdcompany.minemodconstructor.fragment.components.InstallButtonRecyclerViewAdapter;
import com.gdcompany.minemodconstructor.fragment.components.InstallStep;
import com.gdcompany.minemodconstructor.fragment.components.ItemDownloadButton;
import com.gdcompany.minemodconstructor.fragment.components.ItemInstallButton;
import com.gdcompany.minemodconstructor.fragment.components.OtherModRecyclerViewAdapter;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;
import com.gdcompany.minemodconstructor.model.ImageContentItem;
import com.gdcompany.minemodconstructor.model.TryOtherModModelItem;
import com.gdcompany.minemodconstructor.utils.IRewardedAdOutput;
import com.gdcompany.minemodconstructor.utils.MyViewSwitcher;
import com.gdcompany.minemodconstructor.utils.RateDialogFragment;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.rd.PageIndicatorView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;


public class InformationFragment extends BasicFragment<InformationFragmentPresenter, InformationFragmentModel> implements DownloadButtonRecyclerViewAdapter.OnDownloadButtonInteractionListener, InstallButtonRecyclerViewAdapter.OnButtonInteractionListener, OtherModRecyclerViewAdapter.onOtherModInteractionListener {

    @BindView(R.id.adLayout)
    FrameLayout adLayout;
    @BindView(R.id.modNameText)
    AppCompatTextView modNameText;
    @BindView(R.id.imageFrame)
    View imageFrame;
    @BindView(R.id.modImage)
    MyViewSwitcher modImage;
    @BindView(R.id.modText)
    AppCompatTextView modText;
    @BindView(R.id.progressBarBackground)
    AppCompatImageView progressBarBackground;
    @BindView(R.id.infoStepOneProgressBar)
    View infoStepOneProgressBar;
    @BindView(R.id.infoStepTwoProgressBar)
    View infoStepTwoProgressBar;
    @BindView(R.id.infoStepThirdProgressBar)
    View infoStepThirdProgressBar;
    @BindView(R.id.downloadStepProgressBar)
    View downloadStepProgressBar;
    @BindView(R.id.installStepProgressBar)
    View installStepProgressBar;
    @BindView(R.id.activateStepProgressBar)
    View activateStepProgressBar;
    @BindView(R.id.progressBarLayout)
    LinearLayout progressBarLayout;
    @BindView(R.id.progressBarStroke)
    AppCompatImageView progressBarStroke;
    @BindView(R.id.infoStepOneIcon)
    AppCompatImageView infoStepOneIcon;
    @BindView(R.id.infoStepTwoIcon)
    AppCompatImageView infoStepTwoIcon;
    @BindView(R.id.infoStepThirdIcon)
    AppCompatImageView infoStepThirdIcon;
    @BindView(R.id.downloadStepIcon)
    AppCompatImageView downloadStepIcon;
    @BindView(R.id.installStepIcon)
    AppCompatImageView installStepIcon;
    @BindView(R.id.activateStepIcon)
    AppCompatImageView activateStepIcon;
    @BindView(R.id.progressIconsLayout)
    LinearLayout progressIconsLayout;
    @BindView(R.id.infoStepOneText)
    AppCompatTextView infoStepOneText;
    @BindView(R.id.infoStepTwoText)
    AppCompatTextView infoStepTwoText;
    @BindView(R.id.infoStepThreeText)
    AppCompatTextView infoStepThreeText;
    @BindView(R.id.downloadStepText)
    AppCompatTextView downloadStepText;
    @BindView(R.id.installStepText)
    AppCompatTextView installStepText;
    @BindView(R.id.activateStepText)
    AppCompatTextView activateStepText;
    @BindView(R.id.progressTextLayout)
    LinearLayout progressTextLayout;
    @BindView(R.id.modProgressLayout)
    ConstraintLayout modProgressLayout;
    @BindView(R.id.prevButton)
    AppCompatButton prevButton;
    @BindView(R.id.nextButton)
    AppCompatButton nextButton;
    @BindView(R.id.nextButtonText)
    AppCompatTextView nextButtonText;
    @BindView(R.id.bottomNavigationBarLayout)
    LinearLayout bottomNavigationBarLayout;
    @BindView(R.id.bottomNavigationBarConstraint)
    ConstraintLayout bottomNavigationBarConstraint;
    @BindView(R.id.actionButtonRecycler)
    RecyclerView actionButtonRecycler;
    @BindView(R.id.activateImageOne)
    AppCompatImageView activateImageOne;
    @BindView(R.id.activateImageTwo)
    AppCompatImageView activateImageTwo;
    @BindView(R.id.nextButtonArrow)
    AppCompatImageView nextButtonArrow;
    @BindView(R.id.otherModRecycler)
    RecyclerView otherModRecycler;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.adLoadingAnimation)
    AppCompatImageView adLoadingAnimation;
    @BindView(R.id.messageText)
    AppCompatTextView messageText;
    @BindView(R.id.currentStepText)
    AppCompatTextView currentStepText;
    private View inflateView;
    private Animation inAnimationNextStep;
    private Animation outAnimationNextStep;
    private Animation inAnimationPrevStep;
    private Animation outAnimationPrevStep;
    private AlertDialog.Builder builder;

    private RecyclerView.Adapter actionButtonAdapter;

    private OtherModRecyclerViewAdapter otherModRecyclerViewAdapter;
    private DownloadButtonRecyclerViewAdapter downloadAdapter;

    private Timer fakeFileTimer;
    private boolean isHighDpi = false;

    public InformationFragment() {
        super(new InformationFragmentPresenter());
    }

    public static InformationFragment newInstance() {
        return new InformationFragment();
    }

    @Override
    public void assemblyView() {
        getPresenter().setFragmentView(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        double deviceInchesDiagonal = checkDeviceInchesDiagonal();
        if (inflateView == null) {
            if (deviceInchesDiagonal > 0) {
                if (deviceInchesDiagonal >= 6.8) {
                    inflateView = inflater.inflate(R.layout.fragment_information_tablet, container, false);
                } else {
                    if (deviceInchesDiagonal <=5.5) {
                        isHighDpi = true;
                        inflateView = inflater.inflate(R.layout.fragment_information_high_dpi, container, false);
                    } else {
                        inflateView = inflater.inflate(R.layout.fragment_information, container, false);
                    }
                }
            } else {
                inflateView = inflater.inflate(R.layout.fragment_information, container, false);
            }
        }
        return inflateView;
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().loadNativeAd(requireContext());
        setupAdLoadingAnimation();
        setupNavigationButtons();
        setupManualSwitchStepButtons();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().loadInterstitialAd(getActivity());
        getPresenter().updateStep();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            AppConfigSharedModel appConfigSharedModel = new ViewModelProvider(activity).get(AppConfigSharedModel.class);
            getPresenter().linkSharedViewModel(appConfigSharedModel);
        }
    }

    @Override
    public void updateView(InformationFragmentModel viewModel) {
        boolean isTablet = checkDeviceIsTablet();
        InstallStep currentStep = viewModel.getSharedViewModel().getCurrentStep();
        String modText = viewModel.getSharedViewModel().getInfoStepText();
        String modNameText = viewModel.getSharedViewModel().getModNameText();
        String tryOtherText = getString(R.string.try_other_mods_text);
        updateModName(currentStep, modNameText, tryOtherText);
        updateProgressBar(currentStep);
        updateInfoStepText(modText);
        updateBottomNavigationBar(currentStep);
        switch (currentStep) {
            case STEP_INFO_ONE:
            case STEP_INFO_TWO:
            case STEP_INFO_THREE:
                hideActionButtonRecycler();
                hideActivateImages();
                hideMessageText();
                showProgressBar();
                showModText();
                showModImage();
                //showPageIndicatorView();
                break;
            case STEP_DOWNLOAD:
                if (downloadAdapter == null) {
                    downloadAdapter = new DownloadButtonRecyclerViewAdapter(viewModel.getSharedViewModel().getDownloadList(), this);
                }
                updateActionButtonRecycler(currentStep, downloadAdapter);
                hideActivateImages();
                hideModText();
                hideMessageText();
                hideModImage();
                hideOtherModRecycler();
                showProgressBar();
                showActionButtonRecycler();
                hidePageIndicatorView();
                break;
            case STEP_INSTALL:
                if (downloadAdapter == null) {
                    downloadAdapter = new DownloadButtonRecyclerViewAdapter(viewModel.getSharedViewModel().getDownloadList(), this);
                }
                InstallButtonRecyclerViewAdapter installAdapter = new InstallButtonRecyclerViewAdapter(viewModel.getSharedViewModel().getInstallList(), this);
                updateActionButtonRecycler(currentStep, installAdapter);
                hideActivateImages();
                hideModText();
                hideModImage();
                hideOtherModRecycler();
                hideMessageText();
                showProgressBar();
                showActionButtonRecycler();
                hidePageIndicatorView();
                if (!downloadAdapter.isAllDownloadsComplete()) {
                    hideActionButtonRecycler();
                    showMessageText();
                }
                break;
            case STEP_ACTIVATE:
                hideModText();
                hideModImage();
                hideOtherModRecycler();
                hideActionButtonRecycler();
                hideMessageText();
                showProgressBar();
                showActivateImages();
                hidePageIndicatorView();
                break;
            case STEP_TRY_OTHER_MOD:
                List<TryOtherModModelItem> otherMods = new ArrayList<>();
                if(viewModel.getSharedViewModel().getTryOtherModModel()!=null) {
                    otherMods = viewModel.getSharedViewModel().getTryOtherModModel().getOtherMods();
                }
                hideProgressBar();
                hideModImage();
                hideActivateImages();
                hideActionButtonRecycler();
                hideMessageText();
                otherModRecyclerViewAdapter = new OtherModRecyclerViewAdapter(otherMods, this, isTablet, isHighDpi);
                updateOtherModRecycler(otherModRecyclerViewAdapter);
                showOtherModRecycler();
                hidePageIndicatorView();
                break;
        }
    }

    private void updateBottomNavigationBar(InstallStep currentStep) {
        switch (currentStep) {
            case STEP_ACTIVATE:
                nextButtonText.setText(R.string.more_games_text);
                nextButtonArrow.setVisibility(View.VISIBLE);
                break;
            case STEP_TRY_OTHER_MOD:
                nextButtonText.setText(R.string.rate_us_text);
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new RateDialogFragment().getInstance().onShow(getActivity().getSupportFragmentManager(), "Rate");
                    }
                });
                nextButtonArrow.setVisibility(View.GONE);
                break;
            default:
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getPresenter().nextStep();
                        boolean showAds = getPresenter().wasLoadTimeLessThanNSecondsAgo();
                        if (showAds) {
                            getPresenter().showInterstitialAd(getActivity());
                        }
                    }
                });
                nextButtonText.setText(R.string.next_button_text);
                nextButtonArrow.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateModName(InstallStep currentStep, String modName, String tryOtherText) {
        switch (currentStep) {
            case STEP_TRY_OTHER_MOD:
                modNameText.setText(tryOtherText);
                break;
            default:
                modNameText.setText(modName);
                break;
        }
    }

    private void setupAdLoadingAnimation() {
        //adLoadingAnimation.setVisibility(View.GONE);
        Glide.with(this).load(getString(R.string.loading_gif_path)).into(adLoadingAnimation);
    }

    private void setupManualSwitchStepButtons() {
        infoStepOneIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().switchStep(InstallStep.STEP_INFO_ONE, getActivity());
            }
        });
        infoStepTwoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().switchStep(InstallStep.STEP_INFO_TWO, getActivity());
            }
        });
        infoStepThirdIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().switchStep(InstallStep.STEP_INFO_THREE, getActivity());
            }
        });
        downloadStepIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().switchStep(InstallStep.STEP_DOWNLOAD, getActivity());
            }
        });
        installStepIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().switchStep(InstallStep.STEP_INSTALL, getActivity());
            }
        });
        activateStepIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().switchStep(InstallStep.STEP_ACTIVATE, getActivity());
            }
        });
    }

    public void setupImageSwitcher() {
        modImage.setFactory(new ViewSwitcher.ViewFactory() {

            @Override
            public View makeView() {
                ViewPager viewPager = new ViewPager(requireContext());
                LayoutParams params = new ViewSwitcher.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                viewPager.setLayoutParams(params);
                return viewPager;
            }
        });
        if (modImage.getItemViewPagerAdapter() != null) {
            modImage.getItemViewPagerAdapter().onDestroyTimer();
        }
        modImage.setViewPagerAdapter(getPresenter().getInfoOneImages(), pageIndicatorView, this.getActivity());
    }

    private boolean checkDeviceIsTablet() {
        boolean isTablet = false;
        double diagonalInches = 0;
        Point point = new Point();
        ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRealSize(point);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width=point.x;
        int height=point.y;
        double wi=(double)width/(double)displayMetrics.xdpi;
        double hi=(double)height/(double)displayMetrics.ydpi;
        double x = Math.pow(wi,2);
        double y = Math.pow(hi,2);
        diagonalInches = (Math.round((Math.sqrt(x+y)) * 10.0) / 10.0);
        if (diagonalInches >= 6.8) {
            isTablet = true;
        }
        return isTablet;
    }

    private double checkDeviceInchesDiagonal() {
        double diagonalInches = 0;
        Point point = new Point();
        ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRealSize(point);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int width=point.x;
        int height=point.y;
        double wi=(double)width/(double)displayMetrics.xdpi;
        double hi=(double)height/(double)displayMetrics.ydpi;
        double x = Math.pow(wi,2);
        double y = Math.pow(hi,2);
        diagonalInches = (Math.round((Math.sqrt(x+y)) * 10.0) / 10.0);
        return diagonalInches;
    }

    public void setImageSwitcherResource(List<ImageContentItem> images, boolean isNext, boolean animationEnabled) {
        if (modImage.getItemViewPagerAdapter() != null) {
            modImage.getItemViewPagerAdapter().onDestroyTimer();
        }
        if (isNext) {
            modImage.setInAnimation(inAnimationNextStep);
            modImage.setOutAnimation(outAnimationNextStep);
        } else {
            if (animationEnabled) {
                modImage.setInAnimation(inAnimationPrevStep);
                modImage.setOutAnimation(outAnimationPrevStep);
            } else {
                modImage.setInAnimation(null);
                modImage.setOutAnimation(null);
            }
        }
        modImage.setViewPagerAdapter(images, pageIndicatorView, this.getActivity());
    }

    public void prepareNativeAdView(NativeAd nativeAd, boolean hasVideoContent) {
        if (nativeAd != null) {
            NativeAdView adView = (NativeAdView) getLayoutInflater()
                    .inflate(R.layout.native_ad_layout, adLayout,false);
            populateNativeAdView(nativeAd, adView);
            adLayout.removeAllViews();
            adLayout.addView(adView);
        }
    }

    private void populateNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        TextView adNotificationView = adView.findViewById(R.id.ad_notification_view);
        MediaView mediaView = adView.findViewById(R.id.appinstall_media);
        adView.setMediaView(mediaView);
        mediaView.setImageScaleType(ImageView.ScaleType.CENTER_INSIDE);
        adView.setHeadlineView(adView.findViewById(R.id.primary));
        adView.setCallToActionView(adView.findViewById(R.id.cta));
        adView.setIconView(adView.findViewById(R.id.icon));
        adView.setStarRatingView(adView.findViewById(R.id.rating_bar));
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
        mediaView.setVisibility(View.VISIBLE);
        adView.getHeadlineView().setVisibility(View.VISIBLE);
        adView.getCallToActionView().setVisibility(View.VISIBLE);
        adView.getIconView().setVisibility(View.VISIBLE);
        adView.getStarRatingView().setVisibility(View.VISIBLE);
        adNotificationView.setVisibility(View.VISIBLE);
    }

    private void setupNavigationButtons() {
        inAnimationNextStep = AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_in_right);
        outAnimationNextStep = AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_out_left);
        inAnimationPrevStep = AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_in_left);
        outAnimationPrevStep = AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_out_right);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().nextStep();
                boolean showAds = getPresenter().wasLoadTimeLessThanNSecondsAgo();
                if (showAds) {
                    getPresenter().showInterstitialAd(getActivity());
                }
            }
        });
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().prevStep();
                boolean showAds = getPresenter().wasLoadTimeLessThanNSecondsAgo();
                if (showAds) {
                    getPresenter().showInterstitialAd(getActivity());
                }
            }
        });
    }

    private void updateActionButtonRecycler(InstallStep currentStep, RecyclerView.Adapter actionButtonAdapter) {
        actionButtonRecycler.setAdapter(actionButtonAdapter);
    }

    private void updateOtherModRecycler(OtherModRecyclerViewAdapter adapter) {
        otherModRecycler.setAdapter(adapter);
    }

    private void showActivateImages() {
        if (activateImageTwo.getVisibility() == View.INVISIBLE || activateImageTwo.getVisibility() == View.GONE) {
            activateImageTwo.setVisibility(View.VISIBLE);
        }
        if (activateImageOne.getVisibility() == View.INVISIBLE || activateImageOne.getVisibility() == View.GONE) {
            activateImageOne.setVisibility(View.VISIBLE);
        }
    }

    private void hideActivateImages() {
        if (activateImageOne.getVisibility() == View.VISIBLE) {
            activateImageOne.setVisibility(View.GONE);
        }
        if (activateImageTwo.getVisibility() == View.VISIBLE) {
            activateImageTwo.setVisibility(View.GONE);
        }
    }

    private void showModImage() {
        if (modImage.getVisibility() == View.INVISIBLE || modImage.getVisibility() == View.GONE) {
            modImage.setVisibility(View.VISIBLE);
        }
    }

    private void showModText() {
        if (modText.getVisibility() == View.INVISIBLE || modText.getVisibility() == View.GONE) {
            modText.setVisibility(View.VISIBLE);
        }
    }

    public void showAdLoadingAnimation() {
        if (adLoadingAnimation.getVisibility() == View.INVISIBLE || adLoadingAnimation.getVisibility() == View.GONE) {
            adLoadingAnimation.setVisibility(View.VISIBLE);
        }
    }

    public void showMessageText() {
        if (messageText.getVisibility() == View.INVISIBLE || messageText.getVisibility() == View.GONE) {
            messageText.setVisibility(View.VISIBLE);
        }
    }

    public void hideMessageText() {
        if (messageText.getVisibility() == View.VISIBLE) {
            messageText.setVisibility(View.GONE);
        }
    }

    public void hideAdLoadingAnimation() {
        if (adLoadingAnimation.getVisibility() == View.VISIBLE) {
            adLoadingAnimation.setVisibility(View.GONE);
        }
    }

    private void showPageIndicatorView() {
        if (pageIndicatorView.getVisibility() == View.INVISIBLE || pageIndicatorView.getVisibility() == View.GONE) {
            pageIndicatorView.setVisibility(View.VISIBLE);
        }
    }

    private void hideModImage() {
        if (modImage.getVisibility() == View.VISIBLE) {
            modImage.setVisibility(View.GONE);
        }
    }

    private void hidePageIndicatorView() {
        if (pageIndicatorView.getVisibility() == View.VISIBLE) {
            pageIndicatorView.setVisibility(View.GONE);
        }
    }

    private void hideModText() {
        if (modText.getVisibility() == View.VISIBLE) {
            modText.setVisibility(View.INVISIBLE);
        }
    }

    private void showActionButtonRecycler() {
        actionButtonRecycler.setVisibility(View.GONE);
        actionButtonRecycler.setVisibility(View.VISIBLE);
    }

    private void showOtherModRecycler() {
        otherModRecycler.setVisibility(View.GONE);
        otherModRecycler.setVisibility(View.VISIBLE);
    }

    private void hideActionButtonRecycler() {
        if (actionButtonRecycler.getVisibility() == View.VISIBLE) {
            actionButtonRecycler.setVisibility(View.VISIBLE);
            actionButtonRecycler.setVisibility(View.GONE);
        }
    }

    private void hideOtherModRecycler() {
        if (otherModRecycler.getVisibility() == View.VISIBLE) {
            otherModRecycler.setVisibility(View.VISIBLE);
            otherModRecycler.setVisibility(View.GONE);
        }
    }

    private void showProgressBar() {
        if (modProgressLayout.getVisibility() == View.INVISIBLE) {
            modProgressLayout.setVisibility(View.GONE);
            modProgressLayout.setVisibility(View.VISIBLE);
        }
    }

    private void hideProgressBar() {
        if (modProgressLayout.getVisibility() == View.VISIBLE) {
            modProgressLayout.setVisibility(View.VISIBLE);
            modProgressLayout.setVisibility(View.INVISIBLE);
        }
    }

    private void updateInfoStepText(String text) {
        modText.setText(text);
    }

    private void updateProgressBar(InstallStep currentStep) {
        Typeface light = ResourcesCompat.getFont(getContext(), R.font.montserrat_light);
        Typeface regular = ResourcesCompat.getFont(getContext(), R.font.montserrat_regular);
        switch (currentStep) {
            case STEP_INFO_ONE:
                currentStepText.setText(R.string.info_step_one_text);
                toggleFade(R.id.infoStepOneProgressBar, infoStepOneProgressBar);
                infoStepOneProgressBar.setVisibility(View.VISIBLE);
                infoStepTwoProgressBar.setVisibility(View.INVISIBLE);
                infoStepThirdProgressBar.setVisibility(View.INVISIBLE);
                downloadStepProgressBar.setVisibility(View.INVISIBLE);
                installStepProgressBar.setVisibility(View.INVISIBLE);
                activateStepProgressBar.setVisibility(View.INVISIBLE);

                infoStepOneIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepTwoIcon.setImageResource(R.drawable.ic_info_step_disabled);
                infoStepThirdIcon.setImageResource(R.drawable.ic_info_step_disabled);
                downloadStepIcon.setImageResource(R.drawable.ic_download_step_disabled);
                installStepIcon.setImageResource(R.drawable.ic_install_step_disabled);
                activateStepIcon.setImageResource(R.drawable.ic_activate_step_disabled);

                infoStepOneText.setTypeface(regular);
                infoStepTwoText.setTypeface(light);
                infoStepThreeText.setTypeface(light);
                downloadStepText.setTypeface(light);
                installStepText.setTypeface(light);
                activateStepText.setTypeface(light);
                break;
            case STEP_INFO_TWO:
                currentStepText.setText(R.string.info_step_two_text);
                infoStepOneProgressBar.setVisibility(View.VISIBLE);
                toggleFade(R.id.infoStepTwoProgressBar, infoStepTwoProgressBar);
                infoStepTwoProgressBar.setVisibility(View.VISIBLE);
                infoStepThirdProgressBar.setVisibility(View.INVISIBLE);
                downloadStepProgressBar.setVisibility(View.INVISIBLE);
                installStepProgressBar.setVisibility(View.INVISIBLE);
                activateStepProgressBar.setVisibility(View.INVISIBLE);

                infoStepOneIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepTwoIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepThirdIcon.setImageResource(R.drawable.ic_info_step_disabled);
                downloadStepIcon.setImageResource(R.drawable.ic_download_step_disabled);
                installStepIcon.setImageResource(R.drawable.ic_install_step_disabled);
                activateStepIcon.setImageResource(R.drawable.ic_activate_step_disabled);

                infoStepOneText.setTypeface(regular);
                infoStepTwoText.setTypeface(regular);
                infoStepThreeText.setTypeface(light);
                downloadStepText.setTypeface(light);
                installStepText.setTypeface(light);
                activateStepText.setTypeface(light);
                break;
            case STEP_INFO_THREE:
                currentStepText.setText(R.string.info_step_third_text);
                infoStepOneProgressBar.setVisibility(View.VISIBLE);
                infoStepTwoProgressBar.setVisibility(View.VISIBLE);
                toggleFade(R.id.infoStepThirdProgressBar, infoStepThirdProgressBar);
                infoStepThirdProgressBar.setVisibility(View.VISIBLE);
                downloadStepProgressBar.setVisibility(View.INVISIBLE);
                installStepProgressBar.setVisibility(View.INVISIBLE);
                activateStepProgressBar.setVisibility(View.INVISIBLE);

                infoStepOneIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepTwoIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepThirdIcon.setImageResource(R.drawable.ic_info_step_enabled);
                downloadStepIcon.setImageResource(R.drawable.ic_download_step_disabled);
                installStepIcon.setImageResource(R.drawable.ic_install_step_disabled);
                activateStepIcon.setImageResource(R.drawable.ic_activate_step_disabled);

                infoStepOneText.setTypeface(regular);
                infoStepTwoText.setTypeface(regular);
                infoStepThreeText.setTypeface(regular);
                downloadStepText.setTypeface(light);
                installStepText.setTypeface(light);
                activateStepText.setTypeface(light);
                break;
            case STEP_DOWNLOAD:
                currentStepText.setText(R.string.download_step_text);
                infoStepOneProgressBar.setVisibility(View.VISIBLE);
                infoStepTwoProgressBar.setVisibility(View.VISIBLE);
                infoStepThirdProgressBar.setVisibility(View.VISIBLE);
                toggleFade(R.id.downloadStepProgressBar, downloadStepProgressBar);
                downloadStepProgressBar.setVisibility(View.VISIBLE);
                installStepProgressBar.setVisibility(View.INVISIBLE);
                activateStepProgressBar.setVisibility(View.INVISIBLE);

                infoStepOneIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepTwoIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepThirdIcon.setImageResource(R.drawable.ic_info_step_enabled);
                downloadStepIcon.setImageResource(R.drawable.ic_download_step_enabled);
                installStepIcon.setImageResource(R.drawable.ic_install_step_disabled);
                activateStepIcon.setImageResource(R.drawable.ic_activate_step_disabled);

                infoStepOneText.setTypeface(regular);
                infoStepTwoText.setTypeface(regular);
                infoStepThreeText.setTypeface(regular);
                downloadStepText.setTypeface(regular);
                installStepText.setTypeface(light);
                activateStepText.setTypeface(light);
                break;
            case STEP_INSTALL:
                currentStepText.setText(R.string.install_step_text);
                infoStepOneProgressBar.setVisibility(View.VISIBLE);
                infoStepTwoProgressBar.setVisibility(View.VISIBLE);
                infoStepThirdProgressBar.setVisibility(View.VISIBLE);
                downloadStepProgressBar.setVisibility(View.VISIBLE);
                toggleFade(R.id.installStepProgressBar, installStepProgressBar);
                installStepProgressBar.setVisibility(View.VISIBLE);
                activateStepProgressBar.setVisibility(View.INVISIBLE);

                infoStepOneIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepTwoIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepThirdIcon.setImageResource(R.drawable.ic_info_step_enabled);
                downloadStepIcon.setImageResource(R.drawable.ic_download_step_enabled);
                installStepIcon.setImageResource(R.drawable.ic_install_step_enabled);
                activateStepIcon.setImageResource(R.drawable.ic_activate_step_disabled);

                infoStepOneText.setTypeface(regular);
                infoStepTwoText.setTypeface(regular);
                infoStepThreeText.setTypeface(regular);
                downloadStepText.setTypeface(regular);
                installStepText.setTypeface(regular);
                activateStepText.setTypeface(light);
                break;
            case STEP_ACTIVATE:
                currentStepText.setText(R.string.activate_step_text);
                infoStepOneProgressBar.setVisibility(View.VISIBLE);
                infoStepTwoProgressBar.setVisibility(View.VISIBLE);
                infoStepThirdProgressBar.setVisibility(View.VISIBLE);
                downloadStepProgressBar.setVisibility(View.VISIBLE);
                installStepProgressBar.setVisibility(View.VISIBLE);
                toggleFade(R.id.activateStepProgressBar, activateStepProgressBar);
                activateStepProgressBar.setVisibility(View.VISIBLE);

                infoStepOneIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepTwoIcon.setImageResource(R.drawable.ic_info_step_enabled);
                infoStepThirdIcon.setImageResource(R.drawable.ic_info_step_enabled);
                downloadStepIcon.setImageResource(R.drawable.ic_download_step_enabled);
                installStepIcon.setImageResource(R.drawable.ic_install_step_enabled);
                activateStepIcon.setImageResource(R.drawable.ic_activate_step_enabled);

                infoStepOneText.setTypeface(regular);
                infoStepTwoText.setTypeface(regular);
                infoStepThreeText.setTypeface(regular);
                downloadStepText.setTypeface(regular);
                installStepText.setTypeface(regular);
                activateStepText.setTypeface(regular);
                break;
            case STEP_TRY_OTHER_MOD:
                currentStepText.setText("");
                break;
        }
    }

    private void toggleFade(int imageViewId, View View) {
        Transition transition = new Fade();
        transition.setDuration(200);
        transition.addTarget(imageViewId);
        TransitionManager.beginDelayedTransition((ViewGroup) View.getParent(), transition);
    }

    @Override
    public void onDownloadButtonInteraction(ItemDownloadButton item, int position) {
        boolean isNetworkAvailable = isNetworkAvailable(requireContext());
        boolean isAnyRewardedAdStarted = downloadAdapter.isAnyRewardedAdStarted();
        if (!isAnyRewardedAdStarted) {
            if (isNetworkAvailable) {
                if (item.isRewardEarned()) {
                    if (!item.isDownloadStarted() && !item.isDownloadCompleted()) {
                        if (!item.isFake()) {
                            downloadFile(item, position);
                        } else {
                            downloadFakeFile(item, position);
                        }
                    } else {
                        if (item.isDownloadCompleted()) {
                            //showOptionsDialog(item);
                        }
                    }
                } else {
                    if (!item.isRewardAdStarted()) {
                        item.setRewardAdStarted(true);
                        downloadAdapter.notifyDataSetChanged();
                        getPresenter().loadRewardedAd(getActivity(), requireContext(), item, new IRewardedAdOutput() {
                            @Override
                            public void rewardEarned(ItemDownloadButton item) {
                                item.setRewardAdStarted(false);
                                item.setRewardEarned(true);
                                downloadAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void rewardFailed(String reason) {
                                item.setRewardAdStarted(false);
                                downloadAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            } else {
                showToast(ToastType.ERROR, getString(R.string.no_internet_connection_text), Toast.LENGTH_LONG);
            }
        }
    }

    private void showOptionsDialog(ItemDownloadButton item) {
        builder = new AlertDialog.Builder(requireContext(), R.style.MyDialogTheme);
        builder.setTitle("What would you like to do with:");
        builder.setMessage(String.format("%s.%s %s", item.getFileName(), item.getFileExt(), item.getFileSize()));
        builder.setPositiveButton("Install",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        getPresenter().nextStep();
                    }
                });
        builder.setNeutralButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton("Delete",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final File rootPath = requireContext().getFilesDir();
                        File modFile = new File(rootPath, item.getFileName() + "." + item.getFileExt());
                        if (modFile.exists()) {
                            modFile.delete();
                            getPresenter().removeModFile(modFile);
                        }
                        item.setDownloadCompleted(false);
                        item.setDownloadStarted(false);
                        downloadAdapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.mmcRed));
        dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.mmcBackgroundBlue));
        dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.mmcBackgroundBlue));
    }

    private void downloadFakeFile(ItemDownloadButton item, int position) {
        Random rand = new Random();
        int timerPeriodMax = 700;
        int timerPeriodMin = 200;
        int timerPeriod = rand.nextInt((timerPeriodMax - timerPeriodMin) + 1) + timerPeriodMin;
        final double[] progress = {0};
        int progressStepMax = 25;
        int progressStepMin = 5;
        int progressStep = rand.nextInt((progressStepMax - progressStepMin) + 1) + progressStepMin;
        fakeFileTimer = new Timer();
        fakeFileTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (progress[0] + progressStep < 100) {
                                progress[0] += progressStep;
                                item.setDownloadStarted(true);
                                item.setDownloadProgress(String.format("%.0f", progress[0]));
                            } else {
                                item.setDownloadCompleted(true);
                                item.setDownloadStarted(false);
                                fakeFileTimer.cancel();
                                getPresenter().addFakeFile(position, item);
                            }
                            downloadAdapter.notifyDataSetChanged();
                        }
                    });

                }
            }
        }, 0, timerPeriod);
    }

    private void downloadFile(ItemDownloadButton item, int position) {
        try {
            final File rootPath = requireContext().getFilesDir();
            StorageReference storageReference = item.getStorageReference();
            File modFile = new File(rootPath, item.getFileName() + "." + item.getFileExt());
            if (modFile.exists()) {
                modFile.delete();
            }
            modFile.createNewFile();
            FileDownloadTask fileDownloadTask = storageReference.getFile(modFile);
            getPresenter().addDownloadTask(modFile, fileDownloadTask);
            fileDownloadTask.addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    item.setDownloadCompleted(true);
                    item.setDownloadStarted(false);
                    getPresenter().removeDownloadTask(modFile);
                    getPresenter().addModFile(modFile);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    item.setDownloadCompleted(false);
                    item.setDownloadStarted(false);
                    downloadAdapter.notifyDataSetChanged();
                    getPresenter().removeDownloadTask(modFile);
                    if (modFile.exists()) {
                        modFile.delete();
                    }
                    showToast(ToastType.ERROR, getString(R.string.on_download_failure_text), Toast.LENGTH_LONG);
                }
            }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull FileDownloadTask.TaskSnapshot snapshot) {
                    double progress = 0;
                    progress = (100.0 * snapshot.getBytesTransferred()) / snapshot.getTotalByteCount();
                    long bytes = snapshot.getBytesTransferred();
                    item.setDownloadStarted(true);
                    item.setDownloadProgress(String.format("%.0f", progress));
                    downloadAdapter.notifyDataSetChanged();
                }
            }).addOnCanceledListener(new OnCanceledListener() {
                @Override
                public void onCanceled() {
                    item.setDownloadCompleted(false);
                    item.setDownloadStarted(false);
                    downloadAdapter.notifyDataSetChanged();
                    getPresenter().removeDownloadTask(modFile);
                    if (modFile.exists()) {
                        modFile.delete();
                    }
                    showToast(ToastType.ERROR, getString(R.string.on_download_server_canceled_request_text), Toast.LENGTH_LONG);
                }
            }).addOnPausedListener(new OnPausedListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onPaused(@NonNull FileDownloadTask.TaskSnapshot snapshot) {
                    showToast(ToastType.ERROR, getString(R.string.on_download_paused_text), Toast.LENGTH_LONG);
                }
            });
        } catch (Exception e) {
            item.setDownloadCompleted(false);
            item.setDownloadStarted(false);
            downloadAdapter.notifyDataSetChanged();
            showToast(ToastType.ERROR, String.format(getString(R.string.on_download_exception_text), e.getLocalizedMessage()), Toast.LENGTH_LONG);
            e.printStackTrace();
        }
    }

    @Override
    public void onButtonInteraction(ItemInstallButton item) {
        if (item.getButtonType() == ActionButtonType.INSTALL) {
            installMod(item.getModFile());
        }
        if(item.getButtonType() == ActionButtonType.RATE_US) {
            new RateDialogFragment().getInstance().onShow(getActivity().getSupportFragmentManager(), "Rate");
        }
    }

    @Override
    public void onOtherModInteraction(TryOtherModModelItem item) {
        openGooglePlayStore(item.getLink());
    }

    private void openGooglePlayStore(String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW)
                .setData(Uri.parse("https://play.google.com/store/apps/details?id=" + link)).setPackage("com.android.vending");
        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException exception) {
            showToast(ToastType.ERROR, exception.getLocalizedMessage(), Toast.LENGTH_LONG);
            //startActivity(intent);
        }
    }

    public void installMod(File modFile) {
        try {
            Uri uri = FileProvider.getUriForFile(requireContext(), requireContext().getPackageName() + ".provider", modFile);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "application/octet-stream");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } catch (Exception e) {
            showToast(ToastType.INFO, getString(R.string.minecraft_pe_not_installed_text), Toast.LENGTH_LONG);
        }
    }
}