package com.gdcompany.minemodconstructor.fragment.screens.loading;

import com.gdcompany.minemodconstructor.basic.BasicFragmentInteractor;

interface ILoadingFragmentInteractorOutput {
}

public class LoadingFragmentInteractor extends BasicFragmentInteractor<LoadingFragmentPresenter> {

    public LoadingFragmentInteractor(LoadingFragmentPresenter presenter) {
        super(presenter);
    }
}