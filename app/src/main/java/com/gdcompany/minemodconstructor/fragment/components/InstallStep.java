package com.gdcompany.minemodconstructor.fragment.components;

public enum InstallStep {
    STEP_INFO_ONE(1),
    STEP_INFO_TWO(2),
    STEP_INFO_THREE(3),
    STEP_DOWNLOAD(4),
    STEP_INSTALL(5),
    STEP_ACTIVATE(6),
    STEP_TRY_OTHER_MOD(7);

    private final int stepId;

    InstallStep(int stepId) {
        this.stepId = stepId;
    }

    public int getStepId() {
        return stepId;
    }

    public static InstallStep fromStepId(int stepId) {
        switch (stepId) {
            case 2:
                return STEP_INFO_TWO;
            case 3:
                return STEP_INFO_THREE;
            case 4:
                return STEP_DOWNLOAD;
            case 5:
                return STEP_INSTALL;
            case 6:
                return STEP_ACTIVATE;
            case 7:
                return STEP_TRY_OTHER_MOD;
            default:
                return STEP_INFO_ONE;
        }
    }
}
