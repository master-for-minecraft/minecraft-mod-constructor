package com.gdcompany.minemodconstructor.fragment.components;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.gdcompany.minemodconstructor.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DownloadButtonRecyclerViewAdapter extends RecyclerView.Adapter<DownloadButtonRecyclerViewAdapter.DownloadViewHolder> {

    private final List<ItemDownloadButton> mValues;
    private final OnDownloadButtonInteractionListener mListener;


    public DownloadButtonRecyclerViewAdapter(List<ItemDownloadButton> items, OnDownloadButtonInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NotNull
    @Override
    public DownloadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext().getApplicationContext())
                .inflate(R.layout.item_blue_button, parent, false);
        return new DownloadViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DownloadViewHolder holder, int position) {
        holder.itemDownloadButton = mValues.get(position);
        holder.bindData(position, mValues.size());
        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onDownloadButtonInteraction(holder.itemDownloadButton, position);
            }
        });
        if(holder.itemDownloadButton.isRewardEarned()) {
            if (null != mListener) {
                mListener.onDownloadButtonInteraction(holder.itemDownloadButton, position);
            }
        }
    }

    @Override
    public int getItemCount() {
        if(mValues!=null) {
            return mValues.size();
        } else {
            return 0;
        }
    }

    public interface OnDownloadButtonInteractionListener {
        void onDownloadButtonInteraction(ItemDownloadButton item, int position);
    }

    public boolean isAnyRewardedAdStarted() {
        boolean isAnyRewardedAdStarted = false;
        if(mValues!=null && !mValues.isEmpty()) {
            for(ItemDownloadButton item : mValues) {
                if(item.isRewardAdStarted()) {
                    isAnyRewardedAdStarted = true;
                    break;
                }
            }
        }
        return isAnyRewardedAdStarted;
    }

    public boolean isAllDownloadsComplete() {
        boolean isAllDownloadsComplete = true;
        if(mValues!=null && !mValues.isEmpty()) {
            for(ItemDownloadButton item : mValues) {
                if(!item.isDownloadCompleted()) {
                    isAllDownloadsComplete = false;
                    break;
                }
            }
        }
        return isAllDownloadsComplete;
    }

    public static class DownloadViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.actionButton)
        AppCompatButton downloadButton;
        @BindView(R.id.actionButtonText)
        AppCompatTextView actionButtonText;
        @BindView(R.id.adsIcon)
        AppCompatImageView adsIcon;

        ItemDownloadButton itemDownloadButton;
        final View mView;

        DownloadViewHolder(View view) {
            super(view);
            this.mView = view;
            ButterKnife.bind(this, view);
        }

        void bindData(int position, int itemCount) {
            String fileSize = itemDownloadButton.getFileSize();
            String fileName = itemDownloadButton.getFileName();
            if (!itemDownloadButton.isDownloadStarted()) {
                String dp = mView.getContext().getString(R.string.download_part_text);
                actionButtonText.setText(String.format(dp, String.valueOf(position+1), String.valueOf(itemCount)));
                downloadButton.setBackgroundDrawable(mView.getContext().getResources().getDrawable(R.drawable.blue_round_corner));
            } else {
                String downloadProgress = itemDownloadButton.getDownloadProgress();
                if (downloadProgress.equals("-0")) {
                    downloadProgress = "0";
                }
                actionButtonText.setText(String.format(mView.getContext().getString(R.string.downloading_progress_text), downloadProgress));
            }
            if (itemDownloadButton.isDownloadCompleted()) {
                String pd = mView.getContext().getString(R.string.part_downloaded_text);
                adsIcon.setVisibility(View.GONE);
                actionButtonText.setText(String.format(pd, String.valueOf(position+1), String.valueOf(itemCount)));
                downloadButton.setBackgroundDrawable(mView.getContext().getResources().getDrawable(R.drawable.green_round_corner));
            }
            if(itemDownloadButton.isRewardAdStarted()) {
                actionButtonText.setText(R.string.please_wait_text);
            }
            if(itemDownloadButton.isRewardEarned()) {
                adsIcon.setVisibility(View.GONE);
            } else {
                adsIcon.setVisibility(View.VISIBLE);
            }
        }
    }
}
