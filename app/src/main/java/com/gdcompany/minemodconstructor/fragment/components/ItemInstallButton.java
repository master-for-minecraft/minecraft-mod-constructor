package com.gdcompany.minemodconstructor.fragment.components;

import java.io.File;

public class ItemInstallButton {
    private final ActionButtonType buttonType;
    private final String fileAbsolutePath;
    private final File modFile;
    private final String buttonText;

    public ItemInstallButton(ActionButtonType actionButtonType, File modFile,  String fileAbsolutePath, String buttonText) {
        this.buttonType = actionButtonType;
        this.fileAbsolutePath = fileAbsolutePath;
        this.buttonText = buttonText;
        this.modFile = modFile;
    }

    public File getModFile() {
        return modFile;
    }

    public ActionButtonType getButtonType() {
        return buttonType;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getFileAbsolutePath() {
        return fileAbsolutePath;
    }
}
