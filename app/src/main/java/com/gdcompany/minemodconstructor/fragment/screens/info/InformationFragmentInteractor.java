package com.gdcompany.minemodconstructor.fragment.screens.info;

import android.app.Activity;
import android.content.Context;

import com.gdcompany.minemodconstructor.basic.BasicFragmentInteractor;
import com.gdcompany.minemodconstructor.fragment.components.ItemDownloadButton;
import com.gdcompany.minemodconstructor.instance.MobileAdsInstance;
import com.gdcompany.minemodconstructor.utils.AdConfig;
import com.gdcompany.minemodconstructor.utils.INativeAdsOutput;
import com.gdcompany.minemodconstructor.utils.IRewardedAdOutput;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;

interface IInformationFragmentInteractorOutput {
}

public class InformationFragmentInteractor extends BasicFragmentInteractor<InformationFragmentPresenter> {

    public InformationFragmentInteractor(InformationFragmentPresenter presenter) {
        super(presenter);
    }

    public void loadNativeAds(Context context, INativeAdsOutput callback) {
        MobileAdsInstance.INSTANCE.loadMainScreenNativeAd(context, new INativeAdsOutput() {
            @Override
            public void nativeAdsLoaded(NativeAd nativeAd) {
                callback.nativeAdsLoaded(nativeAd);
            }

            @Override
            public void nativeAdsFailed() {
                callback.nativeAdsFailed();
            }
        }, MobileAdsInstance.AD_NATIVE_MAIN_SCREEN_TEST_ID);
    }

    public void showInterstitialAd(Activity activity) {
        MobileAdsInstance.INSTANCE.showInterstitialAd(activity);
    }

    public void loadInterstitialAd(Activity activity) {
        MobileAdsInstance.INSTANCE.loadInterstitialAd(activity);
    }

    public void loadRewardedAd(Activity activity, Context context, ItemDownloadButton item, IRewardedAdOutput callback) {
        MobileAdsInstance.INSTANCE.loadRewardedAd(activity, context, item, new IRewardedAdOutput() {
            @Override
            public void rewardEarned(ItemDownloadButton item) {
                callback.rewardEarned(item);
            }

            @Override
            public void rewardFailed(String reason) {
                callback.rewardFailed(reason);
            }
        });
    }

}