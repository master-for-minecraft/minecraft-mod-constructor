package com.gdcompany.minemodconstructor.fragment.screens.loading;

import com.gdcompany.minemodconstructor.basic.BasicFragmentModel;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;

public class LoadingFragmentModel extends BasicFragmentModel<AppConfigSharedModel> {

    public LoadingFragmentModel() {

    }

    public LoadingFragmentModel(AppConfigSharedModel viewModel) {
        super(viewModel);
    }

}