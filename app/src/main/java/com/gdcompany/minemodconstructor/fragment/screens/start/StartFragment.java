package com.gdcompany.minemodconstructor.fragment.screens.start;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.basic.BasicFragment;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;


public class StartFragment extends BasicFragment<StartFragmentPresenter, StartFragmentModel> {
    private View inflateView;
    @BindView(R.id.startButton)
    AppCompatButton startButton;
    @BindView(R.id.topAdLayout)
    FrameLayout topAdLayout;

    public StartFragment() {
        super(new StartFragmentPresenter());
    }

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public void assemblyView() {
        getPresenter().setFragmentView(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (inflateView == null) {
            inflateView = inflater.inflate(R.layout.fragment_start, container, false);
        }
        return inflateView;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            AppConfigSharedModel appConfigSharedModel = new ViewModelProvider(activity).get(AppConfigSharedModel.class);
            getPresenter().linkSharedViewModel(appConfigSharedModel);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        topAdLayout.setVisibility(View.INVISIBLE);
        setupStartButton();
        getPresenter().setupAd();
    }

    @Override
    public void updateView(StartFragmentModel viewModel) {
    }

    private void setupStartButton() {
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPresenter().goToInformationFragment();
            }
        });
    }

    public void prepareNativeAdView(NativeAd nativeAd, boolean hasVideoContent) {
        if (nativeAd != null) {
            NativeAdView adView = (NativeAdView) getLayoutInflater()
                    .inflate(R.layout.native_ad_layout, topAdLayout, false);
            populateNativeAdView(nativeAd, adView);
            topAdLayout.removeAllViews();
            topAdLayout.addView(adView);
        } else {
            topAdLayout.setVisibility(View.INVISIBLE);
        }
    }

    private void populateNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        TextView adNotificationView = adView.findViewById(R.id.ad_notification_view);
        MediaView mediaView = adView.findViewById(R.id.appinstall_media);
        adView.setMediaView(mediaView);
        mediaView.setImageScaleType(ImageView.ScaleType.CENTER_INSIDE);
        adView.setHeadlineView(adView.findViewById(R.id.primary));
        adView.setCallToActionView(adView.findViewById(R.id.cta));
        adView.setIconView(adView.findViewById(R.id.icon));
        adView.setStarRatingView(adView.findViewById(R.id.rating_bar));
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
        mediaView.setVisibility(View.VISIBLE);
        adView.getHeadlineView().setVisibility(View.VISIBLE);
        adView.getCallToActionView().setVisibility(View.VISIBLE);
        adView.getIconView().setVisibility(View.VISIBLE);
        adView.getStarRatingView().setVisibility(View.VISIBLE);
        adNotificationView.setVisibility(View.VISIBLE);
        topAdLayout.setVisibility(View.VISIBLE);
    }

}