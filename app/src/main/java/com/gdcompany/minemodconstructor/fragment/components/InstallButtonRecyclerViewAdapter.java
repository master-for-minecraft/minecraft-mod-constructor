package com.gdcompany.minemodconstructor.fragment.components;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.gdcompany.minemodconstructor.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InstallButtonRecyclerViewAdapter extends RecyclerView.Adapter<InstallButtonRecyclerViewAdapter.InstallViewHolder> {

    private final List<ItemInstallButton> mValues;
    private final OnButtonInteractionListener mListener;


    public InstallButtonRecyclerViewAdapter(List<ItemInstallButton> items, OnButtonInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NotNull
    @Override
    public InstallViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext().getApplicationContext())
                .inflate(R.layout.item_blue_button, parent, false);
        return new InstallViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final InstallViewHolder holder, int position) {
        holder.itemInstallButton = mValues.get(position);
        holder.bindData(position);
        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onButtonInteraction(holder.itemInstallButton);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface OnButtonInteractionListener {
        void onButtonInteraction(ItemInstallButton item);
    }

    public static class InstallViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.actionButton)
        AppCompatButton installButton;
        @BindView(R.id.actionButtonText)
        AppCompatTextView actionButtonText;
        @BindView(R.id.adsIcon)
        AppCompatImageView adsIcon;

        ItemInstallButton itemInstallButton;
        final View mView;

        InstallViewHolder(View view) {
            super(view);
            this.mView = view;
            ButterKnife.bind(this, view);
        }

        void bindData(int position) {
           actionButtonText.setText(itemInstallButton.getButtonText());
           adsIcon.setVisibility(View.GONE);
        }
    }
}
