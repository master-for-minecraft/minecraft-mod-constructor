package com.gdcompany.minemodconstructor.fragment.screens.loading;

import android.app.Activity;
import android.content.Context;

import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.basic.BasicFragmentPresenter;
import com.gdcompany.minemodconstructor.instance.FirebaseRemoteConfigInstance;
import com.gdcompany.minemodconstructor.instance.LoaderInstance;
import com.gdcompany.minemodconstructor.instance.MobileAdsInstance;
import com.gdcompany.minemodconstructor.instance.NavigatorInstance;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;
import com.gdcompany.minemodconstructor.model.ModConfigModel;
import com.gdcompany.minemodconstructor.model.TryOtherModModel;
import com.gdcompany.minemodconstructor.utils.AdConfig;
import com.gdcompany.minemodconstructor.utils.IInterstitialAdOutput;
import com.gdcompany.minemodconstructor.utils.INativeAdsOutput;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.nativead.NativeAd;

import java.security.DomainLoadStoreParameter;
import java.util.Locale;

public class LoadingFragmentPresenter extends BasicFragmentPresenter<LoadingFragment, LoadingFragmentInteractor, LoadingFragmentModel> implements ILoadingFragmentInteractorOutput {

    private boolean isLoadingFirebaseRemoteConfig = false;
    private AppConfigSharedModel appConfigSharedModel;

    @Override
    public void assemblyPresenter() {
        interactor = new LoadingFragmentInteractor(this);
        viewModel = new LoadingFragmentModel();
    }

    public void loadFirebaseRemoteConfig(Activity activity, Context context) {
        if (!isLoadingFirebaseRemoteConfig) {
            getFragmentView().enableErrorLayout(false);
            LoaderInstance.INSTANCE_LOADER.show();
            isLoadingFirebaseRemoteConfig = true;
            Locale current = getFragmentView().getResources().getConfiguration().locale;
            if(viewModel!=null && viewModel.getSharedViewModel()!=null) {
                viewModel.getSharedViewModel().setCurrentLocaleString(current.toString());
                viewModel.getSharedViewModel().setCurrentLocale(current);
            }
            FirebaseRemoteConfigInstance.INSTANCE.fetchConfig(new FirebaseRemoteConfigInstance.IFirebaseRemoteConfigOutput() {
                @Override
                public void configFetchSuccess(ModConfigModel modConfigModel, TryOtherModModel tryOtherModModel) {
                    if (viewModel != null && viewModel.getSharedViewModel() != null) {
                        viewModel.getSharedViewModel().setModConfigModel(modConfigModel);
                        viewModel.getSharedViewModel().setTryOtherModModel(tryOtherModModel);
                    }
                    MobileAdsInstance.INSTANCE.loadStartScreenNativeAd(context, new INativeAdsOutput() {
                        @Override
                        public void nativeAdsLoaded(NativeAd nativeAd) {
                            if (nativeAd != null) {
                                if (viewModel != null && viewModel.getSharedViewModel() != null) {
                                    viewModel.getSharedViewModel().setStartScreenNativeAd(nativeAd);
                                }
                            }
                            MobileAdsInstance.INSTANCE.loadInterstitialAd(activity, new IInterstitialAdOutput() {
                                @Override
                                public void interstitialAdLoaded(InterstitialAd interstitialAd) {
                                    isLoadingFirebaseRemoteConfig = false;
                                    LoaderInstance.INSTANCE_LOADER.hide();
                                    goToStartFragment();
                                }

                                @Override
                                public void interstitialAdFailed() {
                                    isLoadingFirebaseRemoteConfig = false;
                                    LoaderInstance.INSTANCE_LOADER.hide();
                                    goToStartFragment();
                                }
                            });
                        }

                        @Override
                        public void nativeAdsFailed() {
                            MobileAdsInstance.INSTANCE.loadInterstitialAd(activity, new IInterstitialAdOutput() {
                                @Override
                                public void interstitialAdLoaded(InterstitialAd interstitialAd) {
                                    isLoadingFirebaseRemoteConfig = false;
                                    LoaderInstance.INSTANCE_LOADER.hide();
                                    goToStartFragment();
                                }

                                @Override
                                public void interstitialAdFailed() {
                                    isLoadingFirebaseRemoteConfig = false;
                                    LoaderInstance.INSTANCE_LOADER.hide();
                                    goToStartFragment();
                                }
                            });

                        }
                    }, MobileAdsInstance.AD_NATIVE_MAIN_SCREEN_TEST_ID);
                }

                @Override
                public void configFetchFailed() {
                    isLoadingFirebaseRemoteConfig = false;
                    if (getFragmentView() != null) {
                        getFragmentView().enableErrorLayout(true);
                    }
                    LoaderInstance.INSTANCE_LOADER.hide();
                }
            });
        }
    }

    private void goToStartFragment() {
        if (NavigatorInstance.INSTANCE.getNavController().getCurrentDestination() != null) {
            if (NavigatorInstance.INSTANCE.getNavController().getCurrentDestination().getId() == R.id.loadingFragment) {
                NavigatorInstance.INSTANCE.getNavController().navigate(R.id.action_loadingFragment_to_startFragment);
            }
        }
    }

    public void linkSharedViewModel(AppConfigSharedModel appConfigSharedModel) {
        viewModel = new LoadingFragmentModel(appConfigSharedModel);
    }

    public AppConfigSharedModel getSharedViewModel() {
        return viewModel.getSharedViewModel();
    }
}