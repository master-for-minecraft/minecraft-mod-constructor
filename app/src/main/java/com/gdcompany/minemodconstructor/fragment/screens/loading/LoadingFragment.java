package com.gdcompany.minemodconstructor.fragment.screens.loading;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.basic.BasicFragment;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;

import butterknife.BindView;


public class LoadingFragment extends BasicFragment<LoadingFragmentPresenter, LoadingFragmentModel> {
    @BindView(R.id.errorText)
    AppCompatTextView errorText;
    @BindView(R.id.retryButton)
    AppCompatButton retryButton;
    private View inflateView;

    public LoadingFragment() {
        super(new LoadingFragmentPresenter());
    }

    public static LoadingFragment newInstance() {
        return new LoadingFragment();
    }

    @Override
    public void assemblyView() {
        getPresenter().setFragmentView(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (inflateView == null) {
            inflateView = inflater.inflate(R.layout.fragment_loading, container, false);
        }
        return inflateView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            AppConfigSharedModel appConfigSharedModel = new ViewModelProvider(activity).get(AppConfigSharedModel.class);
            getPresenter().linkSharedViewModel(appConfigSharedModel);
            boolean isNetworkAvailable = isNetworkAvailable(requireContext());
            setupRetryButton();
            if(isNetworkAvailable && getPresenter().getSharedViewModel()!=null) {
                getPresenter().loadFirebaseRemoteConfig(getActivity(), requireContext());
            } else {
                enableErrorLayout(true);
            }
        }
    }

    @Override
    public void updateView(LoadingFragmentModel viewModel) {
    }

    private void setupRetryButton() {
    	retryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getPresenter().loadFirebaseRemoteConfig(getActivity(),requireContext());
			}
		});
	}

    public void enableErrorLayout(boolean enabled) {
    	if(enabled) {
			retryButton.setVisibility(View.VISIBLE);
			errorText.setVisibility(View.VISIBLE);
		} else {
			retryButton.setVisibility(View.GONE);
			errorText.setVisibility(View.GONE);
		}
	}
}