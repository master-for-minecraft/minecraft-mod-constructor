package com.gdcompany.minemodconstructor.fragment.screens.info;

import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.FragmentActivity;

import com.gdcompany.minemodconstructor.basic.BasicFragmentPresenter;
import com.gdcompany.minemodconstructor.fragment.components.InstallStep;
import com.gdcompany.minemodconstructor.fragment.components.ItemDownloadButton;
import com.gdcompany.minemodconstructor.instance.MobileAdsInstance;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;
import com.gdcompany.minemodconstructor.model.ImageContentItem;
import com.gdcompany.minemodconstructor.utils.INativeAdsOutput;
import com.gdcompany.minemodconstructor.utils.IRewardedAdOutput;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.firebase.storage.FileDownloadTask;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class InformationFragmentPresenter extends BasicFragmentPresenter<InformationFragment, InformationFragmentInteractor, InformationFragmentModel> implements IInformationFragmentInteractorOutput {

    private long loadTime = 0;

    private HashMap<String, FileDownloadTask> downloadTaskHashMap;

    public InformationFragmentPresenter() {
        //loadTime = (new Date()).getTime();
        downloadTaskHashMap = new HashMap<>();
    }

    @Override
    public void assemblyPresenter() {
        interactor = new InformationFragmentInteractor(this);
        viewModel = new InformationFragmentModel();
    }

    public void linkSharedViewModel(AppConfigSharedModel appConfigSharedModel) {
        viewModel = new InformationFragmentModel(appConfigSharedModel);
        getFragmentView().setupImageSwitcher();
    }

    public HashMap<String, FileDownloadTask> getDownloadTaskHashMap() {
        return downloadTaskHashMap;
    }

    public void addDownloadTask(File modFile, FileDownloadTask downloadTask) {
        downloadTaskHashMap.put(modFile.getAbsolutePath(), downloadTask);
    }

    public void removeDownloadTask(File modFile) {
        downloadTaskHashMap.remove(modFile.getAbsolutePath());
    }

    public void updateStep() {
        getFragmentView().updateView(viewModel);
    }

    public void nextStep() {
        int lastStepId = viewModel.getSharedViewModel().getCurrentStep().getStepId();
        /*if (InstallStep.fromStepId(lastStepId) == InstallStep.STEP_DOWNLOAD) {
            if (!getDownloadTaskHashMap().isEmpty()) {
                getFragmentView().showToast(ToastType.INFO, "Please wait until downloads finished", Toast.LENGTH_LONG);
            }
        }*/
        viewModel.getSharedViewModel().nextStep();
        int currentStepId = viewModel.getSharedViewModel().getCurrentStep().getStepId();
        if (currentStepId < InstallStep.STEP_DOWNLOAD.getStepId()) {
            switch (InstallStep.fromStepId(currentStepId)) {
                case STEP_INFO_ONE:
                    getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoOneImages(), true, true);
                    break;
                case STEP_INFO_TWO:
                    getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoTwoImages(), true, true);
                    break;
                case STEP_INFO_THREE:
                    getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoThreeImages(), true, true);
                    break;
            }
        }
        if (lastStepId != currentStepId) {
            getFragmentView().updateView(viewModel);
        }
    }

    public void switchStep(InstallStep step, FragmentActivity activity) {
        boolean showAds = wasLoadTimeLessThanNSecondsAgo();
        if (showAds) {
            showInterstitialAd(activity);
        }
        viewModel.getSharedViewModel().setCurrentStep(step);
        switch (InstallStep.fromStepId(step.getStepId())) {
            case STEP_INFO_ONE:
                getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoOneImages(), true, true);
                break;
            case STEP_INFO_TWO:
                getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoTwoImages(), true, true);
                break;
            case STEP_INFO_THREE:
                getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoThreeImages(), true, true);
                break;
        }
        getFragmentView().updateView(viewModel);
    }

    public void loadNativeAd(Context context) {
        getFragmentView().showAdLoadingAnimation();
        interactor.loadNativeAds(context, new INativeAdsOutput() {
            @Override
            public void nativeAdsLoaded(NativeAd nativeAd) {
                if (nativeAd != null) {
                    if(getFragmentView()!=null) {
                        if (getFragmentView().isAdded()) {
                            getFragmentView().prepareNativeAdView(nativeAd, Objects.requireNonNull(nativeAd.getMediaContent()).hasVideoContent());
                            getFragmentView().hideAdLoadingAnimation();
                        }
                    }
                }
            }

            @Override
            public void nativeAdsFailed() {
                getFragmentView().hideAdLoadingAnimation();
            }
        });
    }

    public void showInterstitialAd(Activity activity) {
        interactor.showInterstitialAd(activity);
    }

    public void loadInterstitialAd(Activity activity) {
        interactor.loadInterstitialAd(activity);
    }

    public void loadRewardedAd(Activity activity, Context context, ItemDownloadButton item, IRewardedAdOutput callback) {
        interactor.loadRewardedAd(activity, context, item, new IRewardedAdOutput() {
            @Override
            public void rewardEarned(ItemDownloadButton item) {
                callback.rewardEarned(item);
            }

            @Override
            public void rewardFailed(String reason) {
                callback.rewardFailed(reason);
            }
        });
    }

    public void prevStep() {
        int lastStepId = viewModel.getSharedViewModel().getCurrentStep().getStepId();
        viewModel.getSharedViewModel().prevStep();
        int currentStepId = viewModel.getSharedViewModel().getCurrentStep().getStepId();
        if (currentStepId != lastStepId) {
            switch (InstallStep.fromStepId(currentStepId)) {
                case STEP_INFO_ONE:
                    getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoOneImages(), false, true);
                    break;
                case STEP_INFO_TWO:
                    getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoTwoImages(), false, true);
                    break;
                case STEP_INFO_THREE:
                    getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoThreeImages(), false, true);
                    break;
            }
        } else {
            getFragmentView().setImageSwitcherResource(viewModel.getSharedViewModel().getInfoOneImages(), false, false);
        }
        getFragmentView().updateView(viewModel);
    }

    public void addModFile(File modFile) {
        if(viewModel!=null && viewModel.getSharedViewModel()!=null) {
            viewModel.getSharedViewModel().addModFile(modFile);
        }
    }

    public void addFakeFile(int position, ItemDownloadButton item) {
        if(viewModel!=null && viewModel.getSharedViewModel()!=null) {
            viewModel.getSharedViewModel().addFakeFile(position, item);
        }
    }

    public void removeModFile(File modFile) {
        if(viewModel!=null && viewModel.getSharedViewModel()!=null) {
            viewModel.getSharedViewModel().removeModFile(modFile);
        }
    }

    public List<ImageContentItem> getInfoOneImages() {
        return viewModel.getSharedViewModel().getInfoOneImages();
    }

    public boolean wasLoadTimeLessThanNSecondsAgo() {
        long now = (new Date()).getTime();
        long dateDifference = now - loadTime;
        boolean showAds = dateDifference > MobileAdsInstance.AD_COOLDOWN;
        loadTime = now;
        return showAds;
    }
}