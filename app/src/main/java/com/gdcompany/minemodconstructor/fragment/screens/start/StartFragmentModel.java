package com.gdcompany.minemodconstructor.fragment.screens.start;

import com.gdcompany.minemodconstructor.basic.BasicFragmentModel;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;

public class StartFragmentModel extends BasicFragmentModel<AppConfigSharedModel> {
    public StartFragmentModel(AppConfigSharedModel viewModel) {
        super(viewModel);
    }

    public StartFragmentModel(){

    }
}