package com.gdcompany.minemodconstructor.fragment.components;

import com.google.firebase.storage.StorageReference;

public class ItemDownloadButton {
    private final ActionButtonType buttonType = ActionButtonType.DOWNLOAD;
    private String fileName;
    private String fileExt;
    private StorageReference storageReference;
    private String fileSize;
    private long fileSizeLong;
    private boolean downloadStarted = false;
    private boolean downloadCompleted = false;
    private boolean rewardEarned = false;
    private boolean rewardAdStarted = false;
    private boolean isFake;
    private String downloadProgress;

    public ItemDownloadButton(String fileName, String fileExt, String fileSize, long fileSizeLong, StorageReference storageReference, boolean isFake) {
        this.fileName = fileName;
        this.fileExt = fileExt;
        this.storageReference = storageReference;
        this.fileSize = fileSize;
        this.fileSizeLong = fileSizeLong;
        this.isFake = isFake;
    }

    public boolean isRewardEarned() {
        return rewardEarned;
    }

    public boolean isRewardAdStarted() {
        return rewardAdStarted;
    }

    public boolean isFake() {
        return isFake;
    }

    public void setRewardAdStarted(boolean rewardAdStarted) {
        this.rewardAdStarted = rewardAdStarted;
    }

    public void setRewardEarned(boolean rewardEarned) {
        this.rewardEarned = rewardEarned;
    }

    public long getFileSizeLong() {
        return fileSizeLong;
    }

    public String getFullName() {
        return fileName + "." + fileExt;
    }

    public String getDownloadProgress() {
        return downloadProgress;
    }

    public void setDownloadProgress(String downloadProgress) {
        this.downloadProgress = downloadProgress;
    }

    public boolean isDownloadCompleted() {
        return downloadCompleted;
    }

    public void setDownloadCompleted(boolean downloadCompleted) {
        this.downloadCompleted = downloadCompleted;
    }

    public boolean isDownloadStarted() {
        return downloadStarted;
    }

    public void setDownloadStarted(boolean downloadStarted) {
        this.downloadStarted = downloadStarted;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileExt() {
        return fileExt;
    }

    public StorageReference getStorageReference() {
        return storageReference;
    }

    public String getFileSize() {
        return fileSize;
    }

    public ActionButtonType getButtonType() {
        return buttonType;
    }
}
