package com.gdcompany.minemodconstructor.fragment.components;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.instance.StaticServerConfig;
import com.gdcompany.minemodconstructor.model.TryOtherModModelItem;
import com.gdcompany.minemodconstructor.utils.GlideApp;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OtherModRecyclerViewAdapter extends RecyclerView.Adapter<OtherModRecyclerViewAdapter.OtherModViewHolder> {

    private final List<TryOtherModModelItem> mValues;
    private final onOtherModInteractionListener mListener;
    private final boolean isTablet;
    private final boolean isHighDpi;


    public OtherModRecyclerViewAdapter(List<TryOtherModModelItem> items, onOtherModInteractionListener listener, boolean isTabletDevice, boolean isHighDpiDevice) {
        mValues = items;
        mListener = listener;
        isTablet = isTabletDevice;
        isHighDpi = isHighDpiDevice;
    }

    @NotNull
    @Override
    public OtherModViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(!isTablet) {
            if(!isHighDpi) {
                view = LayoutInflater.from(parent.getContext().getApplicationContext())
                        .inflate(R.layout.item_other_mod, parent, false);
            } else {
                view = LayoutInflater.from(parent.getContext().getApplicationContext())
                        .inflate(R.layout.item_other_mod_high_dpi, parent, false);
            }
        } else {
            view = LayoutInflater.from(parent.getContext().getApplicationContext())
                    .inflate(R.layout.item_other_mod_tablet, parent, false);
        }
        return new OtherModViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final OtherModViewHolder holder, int position) {
        holder.itemOtherMod = mValues.get(position);
        holder.bindData(position);
        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onOtherModInteraction(holder.itemOtherMod);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface onOtherModInteractionListener {
        void onOtherModInteraction(TryOtherModModelItem item);
    }

    public static class OtherModViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.otherModImage)
        AppCompatImageView modImage;
        @BindView(R.id.otherModName)
        AppCompatTextView nameText;

        TryOtherModModelItem itemOtherMod;
        final View mView;

        OtherModViewHolder(View view) {
            super(view);
            this.mView = view;
            ButterKnife.bind(this, view);
        }

        void bindData(int position) {
            CircularProgressDrawable drawable = new CircularProgressDrawable(mView.getContext());
            drawable.setColorSchemeColors(Color.WHITE);
            drawable.setCenterRadius(60f);
            drawable.setStrokeWidth(8f);
            drawable.start();
            String imageName = itemOtherMod.getStorageReference().getName();
            GlideApp.with(mView)
                    .load(StaticServerConfig.STATIC_SERVER_BASE_URL + imageName)
                    .error(
                            GlideApp.with(mView)
                                    .load(itemOtherMod.getStorageReference())
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            return false;
                                        }
                                    })
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .placeholder(drawable)
                    )
                    .placeholder(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(modImage);
            /*GlideApp.with(mView)
                    .load(itemOtherMod.getStorageReference())
                    .placeholder(drawable)
                    .into(modImage);*/
            nameText.setText(itemOtherMod.getName());
        }
    }
}
