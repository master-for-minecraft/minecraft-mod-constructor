package com.gdcompany.minemodconstructor.fragment.screens.start;

import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.basic.BasicFragmentPresenter;
import com.gdcompany.minemodconstructor.fragment.screens.loading.LoadingFragmentModel;
import com.gdcompany.minemodconstructor.instance.NavigatorInstance;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;
import com.google.android.gms.ads.nativead.NativeAd;

import java.util.Objects;

public class StartFragmentPresenter extends BasicFragmentPresenter<StartFragment, StartFragmentInteractor, StartFragmentModel> implements IStartFragmentInteractorOutput {

    @Override
    public void assemblyPresenter() {
        interactor = new StartFragmentInteractor(this);
        viewModel = new StartFragmentModel();
    }

    public void goToInformationFragment() {
        if(NavigatorInstance.INSTANCE.getNavController().getCurrentDestination()!=null) {
            if (NavigatorInstance.INSTANCE.getNavController().getCurrentDestination().getId() == R.id.startFragment) {
                NavigatorInstance.INSTANCE.getNavController().navigate(R.id.action_startFragment_to_informationFragment);
            }
        }
    }

    public void linkSharedViewModel(AppConfigSharedModel appConfigSharedModel) {
        viewModel = new StartFragmentModel(appConfigSharedModel);
    }

    public void setupAd() {
        if(viewModel.getSharedViewModel().getStartScreenNativeAd()!=null) {
            NativeAd nativeAd = viewModel.getSharedViewModel().getStartScreenNativeAd();
            if(getFragmentView().isAdded()) {
                getFragmentView().prepareNativeAdView(nativeAd, Objects.requireNonNull(nativeAd.getMediaContent()).hasVideoContent());
            }
        }
    }
}