package com.gdcompany.minemodconstructor.fragment.components;

public enum ActionButtonType {
    DOWNLOAD(0,"DOWNLOAD"),
    INSTALL(1, "INSTALL"),
    RATE_US(2,"RATE US");

    private final int actionId;
    private final String buttonText;

    ActionButtonType(int actionId, String buttonText) {
        this.actionId = actionId;
        this.buttonText = buttonText;
    }
}
