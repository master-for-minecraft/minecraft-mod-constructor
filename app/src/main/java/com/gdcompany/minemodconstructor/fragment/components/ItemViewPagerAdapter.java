package com.gdcompany.minemodconstructor.fragment.components;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gdcompany.minemodconstructor.BuildConfig;
import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.instance.StaticServerConfig;
import com.gdcompany.minemodconstructor.model.ImageContentItem;
import com.gdcompany.minemodconstructor.utils.GlideApp;
import com.gdcompany.minemodconstructor.utils.IImageLoadStatus;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.widget.ImageView.ScaleType.CENTER;
import static android.widget.ImageView.ScaleType.FIT_XY;

public class ItemViewPagerAdapter extends PagerAdapter implements LifecycleObserver {

    private static final String TAG = ItemViewPagerAdapter.class.getSimpleName();

    private final List<ImageContentItem> list;
    private ViewPager viewPager;

    private Timer mTimer;
    private boolean mTimerStop;
    private final static long DELAY_MS = 3000;// задержка перед выполнением
    private final static long PERIOD_MS = 3000; // период выполнения
    private Runnable mRunnable;
    private Handler mHandler;
    private TimerTask mTimerTask;
    private IImageLoadStatus imageLoadStatus;

    static long lastUpdate = 0;
    int shown = 0;
    long updateInterval = 0;


    public ItemViewPagerAdapter(List<ImageContentItem> list, FragmentActivity activity, IImageLoadStatus iImageLoadStatus) {
        activity.getLifecycle().addObserver(this);
        this.list = list;
        this.imageLoadStatus = iImageLoadStatus;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        CircularProgressDrawable drawable = new CircularProgressDrawable(container.getContext());
        drawable.setColorSchemeColors(Color.WHITE);
        drawable.setCenterRadius(60f);
        drawable.setStrokeWidth(8f);
        drawable.start();
        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        view = layoutInflater.inflate(R.layout.image_view_slider_layout, container, false);
        ImageView mImageView = view.findViewById(R.id.imageViewItem);
        String imageName = list.get(position).getStorageReference().getName();
        GlideApp.with(mImageView)
                .load(StaticServerConfig.STATIC_SERVER_BASE_URL + imageName)
                .error(
                        GlideApp.with(mImageView)
                                .load(list.get(position).getStorageReference())
                                .listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        imageLoadStatus.imageLoadFailed();
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        imageLoadStatus.imageLoadSuccess();
                                        return false;
                                    }
                                })
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(drawable)
                )
                .placeholder(drawable)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mImageView);
        mImageView.setScaleType(FIT_XY);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (container.getParent()!=null) {
            container.removeView((ImageView) object);
        }
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }


    public void onSwipePager() {
        mHandler = new Handler();
        mRunnable = () -> {
            int page = viewPager.getCurrentItem();
            if (page == list.size() - 1) {
                viewPager.setCurrentItem(0, true);
            } else {
                viewPager.setCurrentItem(++page, true);
            }
        };
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                if (!mTimerStop && mHandler != null)
                    mHandler.post(mRunnable);
            }
        };
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, DELAY_MS, PERIOD_MS);
    }

    public void onDestroyTimer() {
        Log.d(TAG, "onStopTimer");
        if (mHandler != null && mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
            mHandler = null;
            mRunnable = null;
        }
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask = null;
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {
        Log.d(TAG, "onResume");
        mTimerStop = false;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onPause() {
        Log.d(TAG, "onPause");
        mTimerStop = true;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        Log.d(TAG, "onPause");
        onDestroyTimer();
    }

}
