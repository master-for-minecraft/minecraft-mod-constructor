package com.gdcompany.minemodconstructor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.basic.BasicActivity;
import com.gdcompany.minemodconstructor.fragment.components.InstallStep;
import com.gdcompany.minemodconstructor.instance.FirebaseRemoteConfigInstance;
import com.gdcompany.minemodconstructor.instance.LoaderInstance;
import com.gdcompany.minemodconstructor.instance.MobileAdsInstance;
import com.gdcompany.minemodconstructor.instance.NavigatorInstance;
import com.gdcompany.minemodconstructor.model.AppConfigSharedModel;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BasicActivity {

    @BindView(R.id.loadingView)
    ImageView loadingAnimation;
    @BindView(R.id.loadingText)
    TextView loadingText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupNavController();
        setupContentLoader();
        AppConfigSharedModel appConfigSharedModel = new ViewModelProvider(this).get(AppConfigSharedModel.class);
        if(savedInstanceState!=null) {
            NavigatorInstance.INSTANCE.getNavController().setGraph(R.navigation.nav_graph);
            appConfigSharedModel.setCurrentStep(InstallStep.STEP_INFO_ONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setupNavController() {
        NavController navController = Navigation.findNavController(MainActivity.this, R.id.activity_main_nav_host_fragment);
        NavigatorInstance.INSTANCE.setNavController(navController);
    }

    private void setupContentLoader() {
        LoaderInstance.INSTANCE_LOADER.setContentLoader(loadingAnimation, loadingText);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof NavHostFragment) {
                NavHostFragment navHostFragment = (NavHostFragment) fragment;
                for (Fragment fragment1 : navHostFragment.getChildFragmentManager().getFragments()) {
                    fragment1.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }
}
