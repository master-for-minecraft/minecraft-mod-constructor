package com.gdcompany.minemodconstructor.basic;

public abstract class BasicFragmentPresenter<FragmentView extends BasicFragment, Interactor extends BasicFragmentInteractor, ViewModel extends BasicFragmentModel> {
	//	private Router router;
	protected Interactor interactor;
	protected ViewModel viewModel;
	private FragmentView fragmentView;

	public BasicFragmentPresenter() {
		assemblyPresenter();
	}

	public abstract void assemblyPresenter();

	protected FragmentView getFragmentView() {
		return fragmentView;
	}

	public void setFragmentView(FragmentView fragmentView) {
		this.fragmentView = fragmentView;
	}

	public void destroyReference() {
		fragmentView = null;
		interactor = null;
		viewModel = null;
	}
}
