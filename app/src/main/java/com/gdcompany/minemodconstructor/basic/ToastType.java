package com.gdcompany.minemodconstructor.basic;

public enum ToastType {
	ERROR,
	SUCCESS,
	WARNING,
	INFO,
	NORMAL
}
