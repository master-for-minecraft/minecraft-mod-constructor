package com.gdcompany.minemodconstructor.basic;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.gdcompany.minemodconstructor.R;
import com.gdcompany.minemodconstructor.activity.MainActivity;
import com.gdcompany.minemodconstructor.utils.LoggerManager;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;

public abstract class BasicFragment<Presenter extends BasicFragmentPresenter, ViewModel extends BasicFragmentModel> extends Fragment implements IBasicContext {

	private static final AtomicInteger lastFragmentId = new AtomicInteger(0);
	private final int fragmentId;

	protected Unbinder unbinder;
	protected BottomNavigationView bottomNavigationView;
	protected Toolbar toolbar;

	@NonNull
	private final Presenter presenter;

	public BasicFragment(@NotNull Presenter presenter) {
		fragmentId = lastFragmentId.incrementAndGet();
		this.presenter = presenter;
		assemblyView();
	}

	@Override
	public void onStart() {
		super.onStart();

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onStart:"));
	}

	@Override
	public void onStop() {
		super.onStop();

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onStop:"));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		presenter.destroyReference();
		LoggerManager.printNavigationInfo(getComponentNameForDebug("onDestroy:"));
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onCreate:"));

        MainActivity appCompatActivity = (MainActivity) getActivity();
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LoggerManager.printNavigationInfo(getComponentNameForDebug("onViewCreated:"));

        unbinder = ButterKnife.bind(this, view);
    }

	@Override
	public void onPause() {
		super.onPause();

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onPause:"));
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onViewStateRestored:"));
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onSaveInstanceState:"));
	}

	@Override
	public void onResume() {
		super.onResume();

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onResume:"));
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		LoggerManager.printNavigationInfo(getComponentNameForDebug("onCreateView:"));

		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onAttach:"));
	}

	@Override
	public void onAttachFragment(@NonNull Fragment childFragment) {
		super.onAttachFragment(childFragment);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onAttachFragment:"));
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onActivityCreated:"));
	}

	public String getComponentNameForDebug(String methodName) {
		return NAVIGATION_DEBUG_LABEL + methodName + this.getClass().getName() + " id:" + fragmentId;
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
		unbinder.unbind();
	}

	@NonNull
	protected Presenter getPresenter() {
		return presenter;
	}

	public void showToast(ToastType toastType, String message, int duration) {
		Context context = getActivity();
		if (context != null)
			switch (toastType) {
				case INFO:
					Toasty.info(context, message, duration, false).show();
					break;
				case ERROR:
					Toasty.error(context, message, duration, false).show();
					break;
				case NORMAL:
					Toasty.normal(context, message, duration, null, false).show();
					break;
				case SUCCESS:
					Toasty.success(context, message, duration, false).show();
					break;
				case WARNING:
					Toasty.warning(context, message, duration, false).show();
					break;

			}
	}

	public boolean isNetworkAvailable(Context context) {
		if(context == null)  return false;


		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connectivityManager != null) {


			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
				NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
				if (capabilities != null) {
					if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
						return true;
					} else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
						return true;
					}  else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)){
						return true;
					}
				}
			}
			else {
				try {
					NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
					if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
						return true;
					}
				} catch (Exception e) {
					Log.i("update_statut", "" + e.getMessage());
				}
			}
		}
		Log.i("update_statut","Network is available : FALSE ");
		return false;
	}

	public abstract void assemblyView();

	public abstract void updateView(ViewModel viewModel);
}
