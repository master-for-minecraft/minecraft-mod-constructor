package com.gdcompany.minemodconstructor.basic;

import androidx.annotation.Keep;
import androidx.lifecycle.ViewModel;
@Keep
public abstract class BasicFragmentModel<SharedViewModel extends ViewModel> {
	private SharedViewModel sharedViewModel;

	public BasicFragmentModel() {
	}

	public BasicFragmentModel(SharedViewModel sharedViewModel) {
		this.sharedViewModel = sharedViewModel;
	}

	public SharedViewModel getSharedViewModel() {
		return sharedViewModel;
	}
}