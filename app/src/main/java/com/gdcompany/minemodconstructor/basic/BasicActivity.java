package com.gdcompany.minemodconstructor.basic;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gdcompany.minemodconstructor.utils.LoggerManager;

import butterknife.ButterKnife;

@SuppressLint("Registered")
public class BasicActivity extends AppCompatActivity implements IBasicContext {

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
		super.onCreate(savedInstanceState, persistentState);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onCreate1:"));

		ButterKnife.bind(this);
	}

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onCreate2:"));
	}

	@Override
	protected void onRestart() {
		super.onRestart();

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onRestart:"));
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
		super.onSaveInstanceState(outState, outPersistentState);

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onSaveInstanceState:"));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onDestroy:"));
	}

	@Override
	protected void onStart() {
		super.onStart();

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onStart:"));
	}

	@Override
	protected void onStop() {
		super.onStop();

		LoggerManager.printNavigationInfo(getComponentNameForDebug("onStop:"));
	}


	private String getComponentNameForDebug(String methodName) {
		return NAVIGATION_DEBUG_LABEL + methodName + this.getLocalClassName();
	}
}
