package com.gdcompany.minemodconstructor.basic;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

public abstract class BasicFragmentInteractor<Presenter extends BasicFragmentPresenter> {
	@NonNull
	private final Presenter presenter;

	public BasicFragmentInteractor(@NotNull Presenter presenter) {
		this.presenter = presenter;
	}

	@NotNull
	public Presenter getPresenter() {
		return presenter;
	}
}
